""" Copyright 2024 Giacomo Luccisano.

This file is part of Gaphor_UML_CubeSat_Stereotypes_Parser.

Gaphor_UML_CubeSat_Stereotypes_Parser is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gaphor_UML_CubeSat_Stereotypes_Parser is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Gaphor_UML_CubeSat_Stereotypes_Parser.  If not, see <http://www.gnu.org/licenses/>.
"""


import xml.etree.ElementTree as ET
import sys
from graphWriter import *
import shutil
from copy import deepcopy

def printModelFile(modelFile: str, ns: dict[str, str], root: ET.Element, tree: ET.ElementTree, 
                   stereotypes: list[dict[str, any]], cleanComps: dict[str, dict[str, any]]) -> None:

    """ 
    Updates the modelFile.gaphor file with cleanComps data.

    Input:
    - modelFile: str with name of file to edit
    - ns: dict with file namespace
    - root: ET.Element variable with name of the root element of the .xml file.
    - tree: ET.ElementTree variable with tree parsed from .xml file.
    - stereotypes: list with stereotypes present in the model.
    - cleanComps: dict with updated model elements.

    Output:
    - none
    """

    for st in stereotypes:       
        for ref in st['reflist']:
            for el in root.findall(".//gNS:Block[gNS:appliedStereotype]", ns):
                if el.find("gNS:appliedStereotype",ns).find("gNS:reflist",ns).find("gNS:ref",ns).attrib['refid'] == ref:
                    compName = rmS(el.find("gNS:name",ns).find("gNS:val",ns).text)

                    for slot in root.findall(".//gNS:Slot[gNS:owningInstance]",ns):
                        if slot.find("gNS:owningInstance",ns).find("gNS:ref",ns).attrib['refid'] == ref:
                            defId = slot.find("gNS:definingFeature",ns).find("gNS:ref",ns).attrib['refid']
                            defProp = root.find(".//gNS:Property[@id='"+ defId + "']",ns)
                            feat = defProp.find("gNS:name",ns).find("gNS:val",ns).text
                            if st['name'] == 'OperatingMode' and feat == 'modeID':
                                slot.find("gNS:value",ns).find("gNS:val",ns).text = str(int(cleanComps[st['name']][compName][feat]))
                            elif feat in ['modeIsOn', 'modeIsStb', 'modeIsPeak']:
                                buffer = '['
                                count = 1
                                for om in cleanComps[st['name']][compName][feat]:
                                    if count < len(cleanComps[st['name']][compName][feat]):
                                        buffer += om + ', '
                                        count += 1
                                    else: 
                                        buffer += om + ']'
                                slot.find("gNS:value",ns).find("gNS:val",ns).text = buffer
                            else:
                                slot.find("gNS:value",ns).find("gNS:val",ns).text = str(cleanComps[st['name']][compName][feat])

    # ET.register_namespace('', ns['gNS'])

    # tree.write(modelFile, xml_declaration=True, encoding="utf-8")
    tree.write("test.gaphor", xml_declaration=True, encoding="utf-8")

# ======================================================================================

def inheritCompProp(component: dict[str, dict[str, any]], sysC: str, prop:str) -> dict[str, dict[str, any]]:
    """ 
    Transfers a prop from a SysComp to a SysComp element.
     
    Input:
    - component: dict with structure from gaphor parser. 
    - sysC: str with SystemComp element key.
    - prop: str with property to inherit.
    
    Output:
    - component updated."""

    component['SystemComp'][sysC][prop] = 0
    idList = []
    mult = []
    for comp in component['SystemComp'][sysC]['SubElements']:
        idList.append(component['SystemComp'][sysC]['SubElements'][comp]['id'])
        mult.append(component['SystemComp'][sysC]['SubElements'][comp]['multiplicity'])
        count = 0
    for id in idList:
        for sysC2 in component['SystemComp']:
            if component['SystemComp'][sysC2]['id'] == id and sysC2 != sysC:
                try:
                    margin = 1 + component['SystemComp'][sysC2]['margin']
                except KeyError:
                    margin = 1
                component['SystemComp'][sysC][prop] += component['SystemComp'][sysC2][prop]*margin*mult[count]
        count += 1
    try:
        margin = 1 + component['SystemComp'][sysC]['margin']
    except KeyError:
        margin = 1
    component['SystemComp'][sysC][prop] = component['SystemComp'][sysC][prop]*margin
        
    return component

# ======================================================================================

def inheritMainProp(component: dict[str, dict[str, any]], sysM: str, prop:str) -> dict[str, dict[str, any]]:
    """ 
    Transfers a prop from a SysComp to a SysMain element.
     
    Input:
    - component: dict with structure from gaphor parser. 
    - sysM: str with SystemMain element key.
    - prop: str with property to inherit.
    
    Output:
    - component updated."""

    component['SystemMain'][sysM][prop] = 0
    idList = []
    mult  = []
    for comp in component['SystemMain'][sysM]['SubElements']:
        idList.append(component['SystemMain'][sysM]['SubElements'][comp]['id'])
        mult.append(component['SystemMain'][sysM]['SubElements'][comp]['multiplicity'])
        count = 0
    for id in idList:
        for sysC in component['SystemComp']:
            if component['SystemComp'][sysC]['id'] == id:
                    
                # inherit masses and power consumptions from SysComp to SysComp
                if 'SubElements' in component['SystemComp'][sysC].keys():
                    component = inheritCompProp(component, sysC, prop)

                try:
                    margin = 1 + component['SystemComp'][sysC]['margin']
                except KeyError:
                    margin = 1
                component['SystemMain'][sysM][prop] += component['SystemComp'][sysC][prop]*margin*mult[count]
        count += 1
    try:
        margin = 1 + component['SystemMain'][sysM]['margin']
    except KeyError:
        margin = 1
    component['SystemMain'][sysM][prop] = component['SystemMain'][sysM][prop]*margin
        
    return component

# ======================================================================================

def deleteUnits(tbc: dict[str, dict[str, any]]) -> dict[str, dict[str, any]]:
    """ 
    Removes all units from a dict. 
    
    Input:
    - tbc: dict with units.
    
    Output:
    - cleanDict: dict without units."""

    cleanDict = deepcopy(tbc)

    for key,value in tbc.items():
        for key_1,value_1 in value.items():
            if type(value_1) is str:
                if ' [' in value_1 :
                    new_value = value_1[:-1].split(' [')[0]
                    if '[' in new_value:
                        cleanDict[key][key_1] = new_value.strip('][').split(', ')
                    elif new_value == 'True' or new_value == 'False':
                        cleanDict[key][key_1] = new_value == 'True'
                    else:
                        try:
                            cleanDict[key][key_1] = float(new_value)
                        except ValueError:
                            cleanDict[key][key_1] = new_value
                else:
                    cleanDict[key][key_1] = value_1
            else:
                cleanDict[key][key_1] = value_1

    return cleanDict

# ======================================================================================

def rmS(string: str) -> str:
    """ 
    Removes spaces from a string. 
    """
    
    return string.replace(" ", "")

# ======================================================================================

def getRequirements(ns: dict[str, str], root: ET.Element) -> list[dict[str, str]]:
    """ 
    Gives all requirements. 
    
    Input: 
    - ns: xml namespace.
    - root: ET.Element variable with name of the root element of the .xml file.
    
    Output:
    - requirementList: list of dict containing requirements names, ids and text.
    """

    requirementList = []

    for req in root.findall('gNS:Requirement', ns):
        id = req.find('gNS:externalId', ns).find('gNS:val', ns).text        
        name = req.find('gNS:name', ns).find('gNS:val', ns).text
        text = req.find('gNS:text', ns).find('gNS:val', ns).text
        requirementList.append({'id':id, 'name':name, 'text':text})

    requirementList = sorted(requirementList, key=lambda x: x['id'])

    return requirementList

# ======================================================================================

def printReqList(requirements: dict[str, str], outPath: str) -> None:
    """ 
    Prints all requirements in the "requirement_list.html" file.
    
    Input: 
    - requirements: list of dict containing requirements names, ids and text.
    - outPath: string with desired output file path.
    
    Output:
    - none
    """

    html = ET.Element('html')
    head = ET.Element('head')
    html.append(head)
    link = ET.Element('link', attrib={'rel': 'stylesheet', 'href': 'table.css'})
    shutil.copy('./table.css', outPath )
    head.append(link)
    body = ET.Element('body')
    div = ET.Element('div')
    table = ET.Element('table')
    html.append(body)
    body.append(div)
    div.append(table)
    tr = ET.Element('tr', attrib={'class': 'header'})
    table.append(tr)
    th = ET.Element('th')
    tr.append(th)
    th.text = 'Req. ID'
    th = ET.Element('th')
    tr.append(th)
    th.text = 'Req. Name'
    th = ET.Element('th')
    tr.append(th)
    th.text = 'Req. Text'

    var = False
    for i in range(len(requirements)):

        if var:
            tr = ET.Element('tr', attrib={'class': 'var'})
            var = False
        else:
            tr = ET.Element('tr')
            var = True
            
        table.append(tr)
        th = ET.Element('th')
        tr.append(th)
        th.text = requirements[i]['id']
        th = ET.Element('th')
        tr.append(th)
        th.text = requirements[i]['name']
        th = ET.Element('th')
        tr.append(th)
        th.text = requirements[i]['text']

    file  = open(outPath+'requirement_list.html', "w+")

    if sys.version_info < (3, 0, 0):
        ET.ElementTree(html).write(file, encoding='utf-8', method='html')
    else:
        ET.ElementTree(html).write(file, encoding='unicode', method='html')

# ======================================================================================

def getStereotypes(ns: dict[str, str], root: ET.Element) -> list[dict[str, any]]:
    """ 
    Gives all stereotypes. 
    
    Input: 
    - ns: xml namespace.
    - root: ET.Element variable with name of the root element of the .xml file.
    
    Output:
    - stereotypes: list of dict containing stereotypes names, ids, refid of components with stereotype assigned.
    """
    
    stereotypes = []

    for st in root.findall('gNS:Stereotype', ns):
        name = rmS(st.find('gNS:name', ns).find('gNS:val', ns).text)
        id = st.attrib['id']
        refList = []
        
        try:
            ref_root = st.find('gNS:instanceSpecification', ns).find('gNS:reflist', ns)
            

            for ref in ref_root.findall('gNS:ref', ns):
                refList.append(ref.attrib['refid'])
        except AttributeError:
            print(name + ' stereotype error: there are no elements with applied stereotype!')

        stereotypes.append({'name': name, 'id': id, 'reflist': refList})

    return stereotypes

# ======================================================================================

def getComponents(ns: dict[str, str], root: ET.Element, stereotype: dict[str, any]) -> dict[str, dict[str, any]]:
    """ 
    Gives all components with assigned stereotype. 
    
    Input: 
    - ns: xml namespace.
    - root: ET.Element variable with name of the root element of the .xml file.
    - stereotype: dict containing stereotype name, id and refid of the components to be searched.
    
    Output:
    - components: list of dict containing components names, ids, properties.
    """

    components = {}

    for ref in stereotype['reflist']:
        for el in root.findall(".//gNS:Block[gNS:appliedStereotype]", ns):
            if el.find("gNS:appliedStereotype",ns).find("gNS:reflist",ns).find("gNS:ref",ns).attrib['refid'] == ref:
                compName = rmS(el.find("gNS:name",ns).find("gNS:val",ns).text)
                components[compName] = {}

                components[compName]['id'] = el.attrib['id']
                
                for slot in root.findall(".//gNS:Slot[gNS:owningInstance]",ns):
                    if slot.find("gNS:owningInstance",ns).find("gNS:ref",ns).attrib['refid'] == ref:
                        defId = slot.find("gNS:definingFeature",ns).find("gNS:ref",ns).attrib['refid']
                        defProp = root.find(".//gNS:Property[@id='"+ defId + "']",ns)
                        feat = defProp.find("gNS:name",ns).find("gNS:val",ns).text
                        components[compName][feat] = slot.find("gNS:value",ns).find("gNS:val",ns).text + \
                                    " [" + defProp.find("gNS:typeValue",ns).find("gNS:val",ns).text + "]"

                components[compName] = dict(sorted(components[compName].items()))

    return components

# ======================================================================================

def getSubElements(ns: dict[str, str], root: ET.Element, sysMain: dict[str, any]) -> dict[str, str]:
    """ 
    Gives all sub components of assigned main system. 
    
    Input: 
    - ns: xml namespace.
    - root: ET.Element variable with name of the root element of the .xml file.
    - sysMain: dict with main system properties.
    
    Output:
    - subElements: dict with ids of sub components.
    """

    subElements = {}

    for aggr in root.findall("gNS:Property[gNS:aggregation]", ns):
        if aggr.find("gNS:aggregation", ns).find("gNS:val", ns).text == 'composite' \
            and aggr.find("gNS:class_", ns).find("gNS:ref", ns).attrib['refid'] == sysMain['id']:
                assId = aggr.find("gNS:association", ns).find("gNS:ref", ns).attrib['refid']
                for ass in root.findall("gNS:AssociationItem[gNS:subject]", ns):
                    if ass.find('gNS:subject', ns).find('gNS:ref', ns).attrib['refid'] == assId:
                        subElRef = ass.find('gNS:tail-connection',ns).find('gNS:ref', ns).attrib['refid']
                        for bl in root.findall("gNS:Block[gNS:appliedStereotype]", ns):
                            if bl.find("gNS:presentation",ns).find("gNS:reflist",ns).find("gNS:ref",ns).attrib['refid'] == subElRef:
                                name = rmS(bl.find("gNS:name",ns).find("gNS:val",ns).text)
                                subElements[name] = {}
                                subElements[name]['id'] = bl.attrib['id']
                                multiplicity = aggr.find('gNS:upperValue',ns).find('gNS:val',ns).text
                                subElements[name]['multiplicity'] = int(multiplicity)

    return subElements