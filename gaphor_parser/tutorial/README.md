# Gaphor parser tutorial
_Author: Giacomo Luccisano. Please note that all the parser scripts are to be extensively tested and corrected. If you have any questions of want to signal a bug, you can [write me an e-mail](mailto:giacomo.luccisano@studenti.polito.it)_.

---

Tutorial overview:
1. [General description](./README.md#general-description)
1. [What is Gaphor? Dependencies and installation](./README.md#what-is-gaphor)
2. [Stereotypes definition and application](./README.md#stereotypes-definition)
3. [How the parser works, model exportation](./README.md#model-exportation)

## General description
Hi! With this tutorial you will become more comfortable with the use of [Gaphor](./README.md#whats-gaphor) and learn how to parse a SysML (`.gaphor`) model into a `.yaml` data structure for your analyses :rocket:

First of all, a few useful links:
- UML documentation: [link](https://www.omg.org/spec/UML/2.5.1/About-UML)
- SysML documentation: [link](https://www.omg.org/spec/SysML/2.0/Beta1/About-SysML)
- Gaphor documentation: [link](https://docs.gaphor.org/en/latest/)
    - _coffee machine_ example: [link](https://docs.gaphor.org/en/latest/coffee_machine.html)
    - _coffee machine_ example video: [link](https://youtu.be/PnWKsr2csXg?si=Xl1Kr1l9Jhq3AewE)
- Graphviz documentation: [link](https://graphviz.org/)
- Working example of a Gaphor model to be parsed into a database for NSS applications: [link](https://gitlab.isae-supaero.fr/saclab/cubesatpreliminarydesign/students/intern-2023-giacomo-luccisano-preliminarydesign/-/tree/main/models/gaphor/heo_model) 

## What is Gaphor?
As from Gaphor documentation:

> Gaphor is a **UML, SysML**, RAAML, and C4 modeling application. It is designed to be **easy to use, while still being powerful**. Gaphor implements a fully-compliant UML 2 data model, so it is much more than a picture drawing tool. You can use Gaphor to quickly visualize different aspects of a system as well as create complete, highly complex models.

The bold sentences are the main reasons why it has been decided, for a new user, to use Gaphor instead of more complex modelling tools.

### Gaphor installation
Gaphor is available for Windows, Mac and Linux. You can follow the download instructions [here](https://gaphor.org/download/).

:exclamation: **For Linux users** it is recommended to use [Flatpak](https://flatpak.org/). Before installing Gaphor, follow the instructions [here](https://flatpak.org/setup) to install Flatpak.

### Other dependencies
:one: Since the parser is written in Python, you should have at least the **3.10 version**: [here](https://www.python.org/downloads/) the download.

:two: For the mission analysis python scripts, everything can be found in [this folder](https://gitlab.isae-supaero.fr/saclab/cubesatpreliminarydesign/students/intern-2023-giacomo-luccisano-preliminarydesign/-/tree/main/python_scripts) of the repository. It also includes the code main dependencies as NASA GMAT and the Gaphor parser itself.

:exclamation: remember to change the paths references in the `CONFIG_input.yaml` file and in the `runMissionFromPythonInDocker` function inside the `functionsDatabase.py`. Everyhing should be straight forward.

If you want, you can run just the parser and it will generate the database from the model `.gaphor` file you can find it in the [dependencies](https://gitlab.isae-supaero.fr/saclab/cubesatpreliminarydesign/students/intern-2023-giacomo-luccisano-preliminarydesign/-/tree/main/python_scripts/dependencies) folder.

:three:  [Docker](./README.md#docker) is instead needed for the correct execution of the `generalMissionAnalysis.py`.

### Docker
[Here](https://docs.docker.com/) the documentation.

Before starting, a few common questions:

- What is Docker? 
> Docker is a platform designed to help developers build, share, and run container applications. It handles the tedious setup, so you can focus on the code.

- What is a container? [Here](https://docs.docker.com/guides/walkthroughs/what-is-a-container/) the answer.

- How do I install Docker? [Here](https://docs.docker.com/engine/install/) the instructions. It is available for Windows, Mac and Linux. A Desktop version is available [here](https://docs.docker.com/get-docker/).


Once you have Docker installed and the python scripts downloaded, from the main folder you can execute in the terminal

```
cd XXXX/dependencies/GMAT_Application && docker build -t gmat:gui .
```

:exclamation: note that the `.` after `gmat:gui` is mandatory, or the Docker image won't be generated. If you did everything correctly, you should have a `gmat:gui` image waiting to be run.

For doing so, you can go again in your terminal and execute

```
docker run -it --rm -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$DISPLAY gmat:gui bash
/app/R2020a/bin/GMAT_Beta
```

or you can simply run the `generalMissionAnalysis.py`. If you did everything correctly, it should be working.

:question: _Where can I find a list of useful Docker commands?_ [Here](https://codenotary.com/blog/extremely-useful-docker-commands).

Some combinations that help a lot:
- list running containers 
``` 
docker ps 
```
- list all container including stopped container
```
docker ps -a
```
- kill one running container
``` 
docker ps   # get container ID
docker kill containerID
```
- kill all running containers 
```
docker kill $(docker ps -q)
```
- delete all stopped containers 
```
docker rm $(docker ps -a -q)
```
- delete all images 
```
docker rmi $(docker images -q)
```


## Stereotypes definition
:question: _Why SysML?_

Because it allows a mission design to be generated from scratch, starting with requirements and functional analysis and ending with the definition of a physical architecture. A major problem with SysML models is that it can be difficult to integrate them with various analysis tools. This is where the proposed **stereotypes** may come into play.

:question: _What is a stereotype?_

From UML specification:
> _A stereotype defines how an existing metaclass may be extended, and enables the use of platform or domain specific terminology or notation in place of, or in addition to, the ones used for the extended metaclass._

:question: _Why are they useful?_

Because they allow us to predefine a set of **common properties** that can be inherited by specific model elements. These properties can then be generic enough to generate a complete database for defining any type of S/C, orbit, GS and ConOps.

Furthermore, by knowing in advance which stereotypes are present in the model and which properties are inherited by the blocks, it is possible to easily run through the `model.gaphor` file and **extract the desired data** for further integration with NSS applications.

:question: _How do I define a stereotype?_

The proposed stereotypes and their properties are presented in the following. You can use Gaphor for generating a model with them. Make sure to **define all the properties and stereotypes names correctly**, otherwise the parser won't work.

:exclamation: A `baseline.gaphor` file with only the stereotypes definitions is provided in the [examples](./examples/) folder, so that you can start modelling immediately.

:question: _What happens if I want to change a model element such as a system?_

**Nothing**. This proposed formalisation is general enough to allow the generation of any S/C without limitations. The only mandatory step is the definition of the stereotypes as presented in the following figures.

:question: _How can I create a stereotype in Gaphor?_

We will get to it, don't worry :wink:

### Systems stereotypes
<p align="center">
<img src="./figs/system_stereotypes.png" alt="Systems UML stereotypes definition">
</p>

#### `SystemComp`
[`SystemComp`](./README.md#systemcomp) is the base stereotype for a physical element of the S/C; it contains the system’s mass, power consumption (in On, Stand-by and Peak modes) and margins to be applied, together with relevant metadata, such as ID, name and manifacturer.

As from the figure above, further stereotypes have been introduced for specialising [`SystemComp`](./README.md#systemcomp):
- [`SystemMain`](./README.md#systemmain)
- [`Battery`](./README.md#battery--solararray)
- [`SolarArray`](./README.md#battery--solararray)
- [`Transmitter`](./README.md#transmitter--receiver)
- [`Receiver`](./README.md#transmitter--receiver)

##### `SystemMain`
It defines a main SubSystem of the S/C (e.g. EPS, Propulsion System, Payload, ...). In addition to the properties introduced by [`SystemComp`](./README.md#systemcomp), it includes the list of OMs in which the system is in On, Stand-by and Peak mode, the system volume and two Boolean flags for post-processing. 

`computeMass` and `computeCons` specify whether or not the parser should consider the masses and power consumption of the system’s sub-components. These were introduced to allow the generation of a database even in the first design iterations, during which the entire system architecture is not defined.

If `computeMass` and/or `computeCons` are set to True, properties of `systemComp` concatenate through the Composite Associations in the graphs. This concatenation chain is interrupted at the `systemMain` level so that the latter inherit masses and power consumptions of their components.

##### `Battery` & `SolarArray`
They include the battery’s capacity and the power generated from the solar arrays.

##### `Transmitter` & `Receiver`
They define the S/C communication system main properties.

### `OperatingMode`

<p align="center">
<img src="./figs/modes_stereotype.png" alt="Systems UML stereotypes definition">
</p>

Allows the modelisation of the ConOps. Each OM can be defined with one or more activation criteria, useful metadata such as mode name, id, and color for post-processing, together with a `priority` counter. The latter is referred from the parser to define the order of interrogation for the activation criteria, so that a mode with an high priority overrides a mode with a lesser one.

Another important attribute to define is the `isDefault`, which indicates the default OM allocated for the simulation, if any activation criteria are met. 

:exclamation: When defining the Operating Modes, only one can have the `isDefault` attribute set to True.

### `GroundStation`

<p align="center">
<img src="./figs/ground_station_stereotype.png" alt="Systems UML stereotypes definition" >
</p>

It defines the main attributes for the modelisation of a Ground Station, from its location to its antenna properties.

### `Orbit` & `PropagationLosses`

<p align="center">
<img src="./figs/orbit_stereotypes.png" alt="Systems UML stereotypes definition" >
</p>

Used for defining the orbital parameters and the losses relative to the signal propagation in space.

### Before starting modelling, what to expect from the parser
If you want to test the parser and all other mission analysis scripts, you can run the `generalMissionAnalysis.py` with the `heo_mission.gaphor` example that can be found in the [examples](./examples/) folder (you just have to download the model file and specify its path in the `generalMissionAnalysis.py` $\rightarrow$ `modelFile` variable).

Alternatively, **AFTER installing Conda** ([here](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html) the download options) from the current folder you can run:

```
cd ..   # move to gaphor_parser directory
        # you should be in repo_installation_dir/gaphor_parser

python --version    # check your python version
                    # Python >= 3.10 is needed

conda create --name gaphor python=3.10.12
conda activate gaphor

conda install -c anaconda pyyaml 
conda install pip
pip install graphviz

python3 gaphorParser.py -h    # for some help...

# run the parser with heo_model example
# outputs will be generated in the outputs/ directory
python3 gaphorParser.py -i "examples/heo_model.gaphor" -o "outputs/"
``` 

If everything runs correctly you should have in your `modelOutDir` directory the following structure:
- `requirements_list.html` table with all requirements listed and alphabetically ordered;
- `table.css` style sheet for `requirements_list.html`, you can customise that as you wish;
- `relationGraph.gv` and `relationGraph.gv.pdf` **still WIP**, generated with [Graphviz](https://graphviz.org/) library, highlights the relationships between variables, to better understand what is affected by a change in the model;
- `components` folder:
    - `CommSys.yaml` with transmitter and receiver data;
    - `EPS.yaml` with solar array and battery data;
    - `GroundStation.yaml` with GS location and antenna data;
    - `OperatingMode.yaml` with the definition of the S/C operating modes;
    - `Orbit.yaml` with orbital parameters; 
    - `PropagationLosses.yaml` self explanatory I guess;
    - `SystemComp.yaml` with a list of all elements with `SystemComp` _stereotype_ applied. They are the components of the main systems (e.g., thrusters of the Propulsion System);
    - `SystemMain.yaml` same as `SystemComp.yaml`, but with the parents Systems properties (e.g., Propulsion System, Payload, ...);
        > Note that both `SystemMain.yaml` and `SystemComp.yaml` elements could have a `subElements` property, specifying the sub components from which the system mass and power consumption are inherited (e.g., the Payload has as subElement the _AFS_, while the _AFS_ itself is composed of a _PHMmini_ and a _RAFS_) 


### Time to model, Gaphor GUI explained

<p align="center">
<img src="./figs/guiTuto.png" alt="How to create a stereotype" >
</p>

:question: _How can I create a stereotype in Gaphor?_

In the top-left corner of your GUI switch **profile to UML**, then **create a profile diagram** (actually, it can be also a generic diagram, but in order to stay coherent with UML specification a profile diagram should be used). 

In the diagram insert a **`<<metaclass>>`** object, then a **`<<stereotype>>`**; both can be found under the **Profile tab** in the bottom-left box. In the right box of the screen, switch on **"Show Attributes"** while clicking on the stereotype block, and always there start inserting as many attributes as you like.

Lastly, draw an **`Extension`** arrow from the `<<metaclass>>` to the `<<stereotype>>`. Make sure that the `<<metaclass>>` name is set to **"Class"** and that's it! You just created your first UML stereotype for your model.

### What now?
Now you can switch profile to SysML and create a model by following any methodology. 

When defining the Physical Architecture through Block Definition Diagrams (or general diagrams) make sure to apply the correct stereotype to each block, so that it could be possible to apply the various attributes to each element. An example of a Payload BDD is shown in the figure below.

<p align="center"> <img src="./figs/payload_example.png" alt="Payload BDD example" > </p>

In this example, the `computeMass` is set to True, meaning that the `SystemMain` Payload will inherit the masses of the `SystemComp`s composing it. 

:exclamation: The parser takes into account the multiplicity of each element, expressed by the number near the Composite Associations in the diagram.

### I have only the Requirements in the model, can I extract them?
Yes! You can call the `getRequirements` and `printReqList` functions from the parser and obtain your requirements table!

### I changed some values in the `components` dictionary, can I change the model?
Yes! You have to clear the `components` dictionary from all measurement units by calling the `deleteUnits` function, then call the `printModelFile` function and that's it! The parser will generate a model file with your modifications.