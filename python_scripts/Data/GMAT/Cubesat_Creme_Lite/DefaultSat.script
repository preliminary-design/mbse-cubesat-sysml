%General Mission Analysis Tool(GMAT) Script
%Created: 2020-11-05 15:34:02


%----------------------------------------
%---------- Spacecraft
%----------------------------------------

Create Spacecraft DefaultSat;
GMAT DefaultSat.DateFormat = UTCGregorian;
GMAT DefaultSat.Epoch = '01 Jan 2023 00:00:00.000';
GMAT DefaultSat.CoordinateSystem = EarthMJ2000Eq;
GMAT DefaultSat.DisplayStateType = Keplerian;
GMAT DefaultSat.SMA = 6978.139999999981;
GMAT DefaultSat.ECC = 0.001059200000000172;
GMAT DefaultSat.INC = 97.802307;
GMAT DefaultSat.RAAN = 190.0962;
GMAT DefaultSat.AOP = 89.99979000014623;
GMAT DefaultSat.TA = 269.878833999852;
GMAT DefaultSat.DryMass = 4;
GMAT DefaultSat.Cd = 2.2;
GMAT DefaultSat.Cr = 1;
GMAT DefaultSat.DragArea = 0.0362;
GMAT DefaultSat.SRPArea = 0.0362;
GMAT DefaultSat.SPADDragScaleFactor = 1;
GMAT DefaultSat.SPADSRPScaleFactor = 1;
GMAT DefaultSat.NAIFId = -10000001;
GMAT DefaultSat.NAIFIdReferenceFrame = -9000001;
GMAT DefaultSat.OrbitColor = Red;
GMAT DefaultSat.TargetColor = Teal;
GMAT DefaultSat.OrbitErrorCovariance = [ 1e+70 0 0 0 0 0 ; 0 1e+70 0 0 0 0 ; 0 0 1e+70 0 0 0 ; 0 0 0 1e+70 0 0 ; 0 0 0 0 1e+70 0 ; 0 0 0 0 0 1e+70 ];
GMAT DefaultSat.CdSigma = 1e+70;
GMAT DefaultSat.CrSigma = 1e+70;
GMAT DefaultSat.Id = 'SatId';
GMAT DefaultSat.Attitude = CoordinateSystemFixed;
GMAT DefaultSat.SPADSRPInterpolationMethod = Bilinear;
GMAT DefaultSat.SPADSRPScaleFactorSigma = 1e+70;
GMAT DefaultSat.SPADDragInterpolationMethod = Bilinear;
GMAT DefaultSat.SPADDragScaleFactorSigma = 1e+70;
GMAT DefaultSat.ModelFile = 'aura.3ds';
GMAT DefaultSat.ModelOffsetX = 0;
GMAT DefaultSat.ModelOffsetY = 0;
GMAT DefaultSat.ModelOffsetZ = 0;
GMAT DefaultSat.ModelRotationX = 0;
GMAT DefaultSat.ModelRotationY = 0;
GMAT DefaultSat.ModelRotationZ = 0;
GMAT DefaultSat.ModelScale = 1;
GMAT DefaultSat.AttitudeDisplayStateType = 'Quaternion';
GMAT DefaultSat.AttitudeRateDisplayStateType = 'AngularVelocity';
GMAT DefaultSat.AttitudeCoordinateSystem = EarthMJ2000Eq;
GMAT DefaultSat.EulerAngleSequence = '321';

%----------------------------------------
%---------- GroundStations
%----------------------------------------

Create GroundStation DefaultGS;
GMAT DefaultGS.OrbitColor = Thistle;
GMAT DefaultGS.TargetColor = DarkGray;
GMAT DefaultGS.CentralBody = Earth;
GMAT DefaultGS.StateType = Spherical;
GMAT DefaultGS.HorizonReference = Ellipsoid;
GMAT DefaultGS.Location1 = 43.6;
GMAT DefaultGS.Location2 = 1.5;
GMAT DefaultGS.Location3 = 0.13;
GMAT DefaultGS.Id = 'StationId';
GMAT DefaultGS.IonosphereModel = 'None';
GMAT DefaultGS.TroposphereModel = 'None';
GMAT DefaultGS.DataSource = 'Constant';
GMAT DefaultGS.Temperature = 295.1;
GMAT DefaultGS.Pressure = 1013.5;
GMAT DefaultGS.Humidity = 55;
GMAT DefaultGS.MinimumElevationAngle = 10;




%----------------------------------------
%---------- ForceModels
%----------------------------------------

Create ForceModel Propagator1_ForceModel;
GMAT Propagator1_ForceModel.CentralBody = Earth;
GMAT Propagator1_ForceModel.PrimaryBodies = {Earth};
GMAT Propagator1_ForceModel.PointMasses = {Luna, Sun};
GMAT Propagator1_ForceModel.SRP = On;
GMAT Propagator1_ForceModel.RelativisticCorrection = Off;
GMAT Propagator1_ForceModel.ErrorControl = RSSStep;
GMAT Propagator1_ForceModel.GravityField.Earth.Degree = 10;
GMAT Propagator1_ForceModel.GravityField.Earth.Order = 10;
GMAT Propagator1_ForceModel.GravityField.Earth.StmLimit = 100;
GMAT Propagator1_ForceModel.GravityField.Earth.PotentialFile = 'JGM2.cof';
GMAT Propagator1_ForceModel.GravityField.Earth.TideModel = 'None';
GMAT Propagator1_ForceModel.SRP.Flux = 1367;
GMAT Propagator1_ForceModel.SRP.SRPModel = Spherical;
GMAT Propagator1_ForceModel.SRP.Nominal_Sun = 149597870.691;
GMAT Propagator1_ForceModel.Drag.AtmosphereModel = JacchiaRoberts;
GMAT Propagator1_ForceModel.Drag.HistoricWeatherSource = 'ConstantFluxAndGeoMag';
GMAT Propagator1_ForceModel.Drag.PredictedWeatherSource = 'ConstantFluxAndGeoMag';
GMAT Propagator1_ForceModel.Drag.CSSISpaceWeatherFile = 'SpaceWeather-All-v1.2.txt';
GMAT Propagator1_ForceModel.Drag.SchattenFile = 'SchattenPredict.txt';
GMAT Propagator1_ForceModel.Drag.F107 = 150;
GMAT Propagator1_ForceModel.Drag.F107A = 150;
GMAT Propagator1_ForceModel.Drag.MagneticIndex = 3;
GMAT Propagator1_ForceModel.Drag.SchattenErrorModel = 'Nominal';
GMAT Propagator1_ForceModel.Drag.SchattenTimingModel = 'NominalCycle';
GMAT Propagator1_ForceModel.Drag.DragModel = 'Spherical';

%----------------------------------------
%---------- Propagators
%----------------------------------------

Create Propagator Propagator1;
GMAT Propagator1.FM = Propagator1_ForceModel;
GMAT Propagator1.Type = RungeKutta89;
GMAT Propagator1.InitialStepSize = 60;
GMAT Propagator1.Accuracy = 9.999999999999999e-09;
GMAT Propagator1.MinStep = 60;
GMAT Propagator1.MaxStep = 60;
GMAT Propagator1.MaxStepAttempts = 50;
GMAT Propagator1.StopIfAccuracyIsViolated = true;

%----------------------------------------
%---------- EventLocators
%----------------------------------------

Create ContactLocator ContactLocator1;
GMAT ContactLocator1.Target = DefaultSat;
GMAT ContactLocator1.Filename = '/app/Output_Lite/ContactLocator1.txt';
GMAT ContactLocator1.InputEpochFormat = 'TAIModJulian';
GMAT ContactLocator1.InitialEpoch = '21545';
GMAT ContactLocator1.StepSize = 600;
GMAT ContactLocator1.FinalEpoch = '21545.138';
GMAT ContactLocator1.UseLightTimeDelay = true;
GMAT ContactLocator1.UseStellarAberration = true;
GMAT ContactLocator1.WriteReport = true;
GMAT ContactLocator1.RunMode = Automatic;
GMAT ContactLocator1.UseEntireInterval = true;
GMAT ContactLocator1.Observers = {DefaultGS};
GMAT ContactLocator1.LightTimeDirection = Transmit;

Create EclipseLocator EclipseLocator1;
GMAT EclipseLocator1.Spacecraft = DefaultSat;
GMAT EclipseLocator1.Filename = '/app/Output_Lite/EclipseLocator1.txt';
GMAT EclipseLocator1.OccultingBodies = {Earth, Luna};
GMAT EclipseLocator1.InputEpochFormat = 'TAIModJulian';
GMAT EclipseLocator1.InitialEpoch = '21545';
GMAT EclipseLocator1.StepSize = 10;
GMAT EclipseLocator1.FinalEpoch = '21545.138';
GMAT EclipseLocator1.UseLightTimeDelay = true;
GMAT EclipseLocator1.UseStellarAberration = true;
GMAT EclipseLocator1.WriteReport = true;
GMAT EclipseLocator1.RunMode = Automatic;
GMAT EclipseLocator1.UseEntireInterval = true;
GMAT EclipseLocator1.EclipseTypes = {'Umbra', 'Penumbra', 'Antumbra'};

%----------------------------------------
%---------- Subscribers
%----------------------------------------

Create OrbitView DefaultOrbitView;
GMAT DefaultOrbitView.SolverIterations = Current;
GMAT DefaultOrbitView.UpperLeft = [ -0.1076470588235294 0.003571428571428571 ];
GMAT DefaultOrbitView.Size = [ 0.9964705882352941 0.9630952380952381 ];
GMAT DefaultOrbitView.RelativeZOrder = 16;
GMAT DefaultOrbitView.Maximized = false;
GMAT DefaultOrbitView.Add = {DefaultSat, Earth, Sun};
GMAT DefaultOrbitView.CoordinateSystem = EarthMJ2000Eq;
GMAT DefaultOrbitView.DrawObject = [ true true true ];
GMAT DefaultOrbitView.DataCollectFrequency = 1;
GMAT DefaultOrbitView.UpdatePlotFrequency = 50;
GMAT DefaultOrbitView.NumPointsToRedraw = 0;
GMAT DefaultOrbitView.ShowPlot = true;
GMAT DefaultOrbitView.MaxPlotPoints = 20000;
GMAT DefaultOrbitView.ShowLabels = true;
GMAT DefaultOrbitView.ViewPointReference = Earth;
GMAT DefaultOrbitView.ViewPointVector = [ 30000 0 0 ];
GMAT DefaultOrbitView.ViewDirection = Earth;
GMAT DefaultOrbitView.ViewScaleFactor = 1;
GMAT DefaultOrbitView.ViewUpCoordinateSystem = EarthMJ2000Eq;
GMAT DefaultOrbitView.ViewUpAxis = Z;
GMAT DefaultOrbitView.EclipticPlane = Off;
GMAT DefaultOrbitView.XYPlane = On;
GMAT DefaultOrbitView.WireFrame = Off;
GMAT DefaultOrbitView.Axes = On;
GMAT DefaultOrbitView.Grid = Off;
GMAT DefaultOrbitView.SunLine = On;
GMAT DefaultOrbitView.UseInitialView = On;
GMAT DefaultOrbitView.StarCount = 7000;
GMAT DefaultOrbitView.EnableStars = On;
GMAT DefaultOrbitView.EnableConstellations = On;

Create GroundTrackPlot DefaultGroundTrackPlot;
GMAT DefaultGroundTrackPlot.SolverIterations = Current;
GMAT DefaultGroundTrackPlot.UpperLeft = [ -0.06411764705882353 0.04880952380952381 ];
GMAT DefaultGroundTrackPlot.Size = [ 0.9964705882352941 0.9630952380952381 ];
GMAT DefaultGroundTrackPlot.RelativeZOrder = 66;
GMAT DefaultGroundTrackPlot.Maximized = false;
GMAT DefaultGroundTrackPlot.Add = {DefaultSat, DefaultGS};
GMAT DefaultGroundTrackPlot.DataCollectFrequency = 1;
GMAT DefaultGroundTrackPlot.UpdatePlotFrequency = 50;
GMAT DefaultGroundTrackPlot.NumPointsToRedraw = 0;
GMAT DefaultGroundTrackPlot.ShowPlot = true;
GMAT DefaultGroundTrackPlot.MaxPlotPoints = 20000;
GMAT DefaultGroundTrackPlot.CentralBody = Earth;
GMAT DefaultGroundTrackPlot.TextureMap = 'ModifiedBlueMarble.jpg';


%----------------------------------------
%---------- Mission Sequence
%----------------------------------------

BeginMissionSequence;
Propagate Propagator1(DefaultSat) {DefaultSat.ElapsedDays = 5};
