%General Mission Analysis Tool(GMAT) Script
%Created: 2020-11-05 15:34:02


%----------------------------------------
%---------- Spacecraft
%----------------------------------------

Create Spacecraft CremeSat;
GMAT CremeSat.DateFormat = UTCGregorian;
GMAT CremeSat.Epoch = '01 Jan 2023 00:00:00.000';
GMAT CremeSat.CoordinateSystem = EarthMJ2000Eq;
GMAT CremeSat.DisplayStateType = Keplerian;
GMAT CremeSat.SMA = 6978.14;
GMAT CremeSat.ECC = 0.0010592;
GMAT CremeSat.INC = 97.82307;
GMAT CremeSat.RAAN = 190.0962;
GMAT CremeSat.AOP = 89.99979;
GMAT CremeSat.TA = -90.121166;
GMAT CremeSat.DryMass = 3.3;
GMAT CremeSat.Cd = 2.2;
GMAT CremeSat.Cr = 1;
GMAT CremeSat.DragArea = 0.0362;
GMAT CremeSat.SRPArea = 0.0362;
GMAT CremeSat.SPADDragScaleFactor = 1;
GMAT CremeSat.SPADSRPScaleFactor = 1;
GMAT CremeSat.NAIFId = -10000001;
GMAT CremeSat.NAIFIdReferenceFrame = -9000001;
GMAT CremeSat.OrbitColor = Red;
GMAT CremeSat.TargetColor = Teal;
GMAT CremeSat.OrbitErrorCovariance = [ 1e+70 0 0 0 0 0 ; 0 1e+70 0 0 0 0 ; 0 0 1e+70 0 0 0 ; 0 0 0 1e+70 0 0 ; 0 0 0 0 1e+70 0 ; 0 0 0 0 0 1e+70 ];
GMAT CremeSat.CdSigma = 1e+70;
GMAT CremeSat.CrSigma = 1e+70;
GMAT CremeSat.Id = 'SatId';
GMAT CremeSat.Attitude = CoordinateSystemFixed;
GMAT CremeSat.SPADSRPInterpolationMethod = Bilinear;
GMAT CremeSat.SPADSRPScaleFactorSigma = 1e+70;
GMAT CremeSat.SPADDragInterpolationMethod = Bilinear;
GMAT CremeSat.SPADDragScaleFactorSigma = 1e+70;
GMAT CremeSat.ModelFile = 'aura.3ds';
GMAT CremeSat.ModelOffsetX = 0;
GMAT CremeSat.ModelOffsetY = 0;
GMAT CremeSat.ModelOffsetZ = 0;
GMAT CremeSat.ModelRotationX = 0;
GMAT CremeSat.ModelRotationY = 0;
GMAT CremeSat.ModelRotationZ = 0;
GMAT CremeSat.ModelScale = 1;
GMAT CremeSat.AttitudeDisplayStateType = 'Quaternion';
GMAT CremeSat.AttitudeRateDisplayStateType = 'AngularVelocity';
GMAT CremeSat.AttitudeCoordinateSystem = EarthMJ2000Eq;
GMAT CremeSat.EulerAngleSequence = '321';

%----------------------------------------
%---------- GroundStations
%----------------------------------------

Create GroundStation Toulouse;
GMAT Toulouse.OrbitColor = Thistle;
GMAT Toulouse.TargetColor = DarkGray;
GMAT Toulouse.CentralBody = Earth;
GMAT Toulouse.StateType = Spherical;
GMAT Toulouse.HorizonReference = Ellipsoid;
GMAT Toulouse.Location1 = 43.6;
GMAT Toulouse.Location2 = 1.5;
GMAT Toulouse.Location3 = 0.13;
GMAT Toulouse.Id = 'StationId';
GMAT Toulouse.IonosphereModel = 'None';
GMAT Toulouse.TroposphereModel = 'None';
GMAT Toulouse.DataSource = 'Constant';
GMAT Toulouse.Temperature = 295.1;
GMAT Toulouse.Pressure = 1013.5;
GMAT Toulouse.Humidity = 55;
GMAT Toulouse.MinimumElevationAngle = 10;


















%----------------------------------------
%---------- ForceModels
%----------------------------------------

Create ForceModel Propagator1_ForceModel;
GMAT Propagator1_ForceModel.CentralBody = Earth;
GMAT Propagator1_ForceModel.PrimaryBodies = {Earth};
GMAT Propagator1_ForceModel.PointMasses = {Luna, Sun};
GMAT Propagator1_ForceModel.SRP = On;
GMAT Propagator1_ForceModel.RelativisticCorrection = Off;
GMAT Propagator1_ForceModel.ErrorControl = RSSStep;
GMAT Propagator1_ForceModel.GravityField.Earth.Degree = 10;
GMAT Propagator1_ForceModel.GravityField.Earth.Order = 10;
GMAT Propagator1_ForceModel.GravityField.Earth.StmLimit = 100;
GMAT Propagator1_ForceModel.GravityField.Earth.PotentialFile = 'JGM2.cof';
GMAT Propagator1_ForceModel.GravityField.Earth.TideModel = 'None';
GMAT Propagator1_ForceModel.SRP.Flux = 1367;
GMAT Propagator1_ForceModel.SRP.SRPModel = Spherical;
GMAT Propagator1_ForceModel.SRP.Nominal_Sun = 149597870.691;
GMAT Propagator1_ForceModel.Drag.AtmosphereModel = JacchiaRoberts;
GMAT Propagator1_ForceModel.Drag.HistoricWeatherSource = 'ConstantFluxAndGeoMag';
GMAT Propagator1_ForceModel.Drag.PredictedWeatherSource = 'ConstantFluxAndGeoMag';
GMAT Propagator1_ForceModel.Drag.CSSISpaceWeatherFile = 'SpaceWeather-All-v1.2.txt';
GMAT Propagator1_ForceModel.Drag.SchattenFile = 'SchattenPredict.txt';
GMAT Propagator1_ForceModel.Drag.F107 = 150;
GMAT Propagator1_ForceModel.Drag.F107A = 150;
GMAT Propagator1_ForceModel.Drag.MagneticIndex = 3;
GMAT Propagator1_ForceModel.Drag.SchattenErrorModel = 'Nominal';
GMAT Propagator1_ForceModel.Drag.SchattenTimingModel = 'NominalCycle';
GMAT Propagator1_ForceModel.Drag.DragModel = 'Spherical';

%----------------------------------------
%---------- Propagators
%----------------------------------------

Create Propagator Propagator1;
GMAT Propagator1.FM = Propagator1_ForceModel;
GMAT Propagator1.Type = RungeKutta89;
GMAT Propagator1.InitialStepSize = 60;
GMAT Propagator1.Accuracy = 9.999999999999999e-12;
GMAT Propagator1.MinStep = 0.001;
GMAT Propagator1.MaxStep = 2700;
GMAT Propagator1.MaxStepAttempts = 50;
GMAT Propagator1.StopIfAccuracyIsViolated = true;

%----------------------------------------
%---------- EventLocators
%----------------------------------------

Create ContactLocator ContactLocator1;
GMAT ContactLocator1.Target = CremeSat;
GMAT ContactLocator1.Filename = '..\..\Python\Data\ContactLocator1.txt';
GMAT ContactLocator1.InputEpochFormat = 'TAIModJulian';
GMAT ContactLocator1.InitialEpoch = '21545';
GMAT ContactLocator1.StepSize = 600;
GMAT ContactLocator1.FinalEpoch = '21545.138';
GMAT ContactLocator1.UseLightTimeDelay = true;
GMAT ContactLocator1.UseStellarAberration = true;
GMAT ContactLocator1.WriteReport = true;
GMAT ContactLocator1.RunMode = Automatic;
GMAT ContactLocator1.UseEntireInterval = true;
GMAT ContactLocator1.Observers = {Toulouse};
GMAT ContactLocator1.LightTimeDirection = Transmit;

Create EclipseLocator EclipseLocator1;
GMAT EclipseLocator1.Spacecraft = CremeSat;
GMAT EclipseLocator1.Filename = '..\..\Python\Data\EclipseLocator1.txt';
GMAT EclipseLocator1.OccultingBodies = {Earth, Luna};
GMAT EclipseLocator1.InputEpochFormat = 'TAIModJulian';
GMAT EclipseLocator1.InitialEpoch = '21545';
GMAT EclipseLocator1.StepSize = 10;
GMAT EclipseLocator1.FinalEpoch = '21545.138';
GMAT EclipseLocator1.UseLightTimeDelay = true;
GMAT EclipseLocator1.UseStellarAberration = true;
GMAT EclipseLocator1.WriteReport = true;
GMAT EclipseLocator1.RunMode = Automatic;
GMAT EclipseLocator1.UseEntireInterval = true;
GMAT EclipseLocator1.EclipseTypes = {'Umbra', 'Penumbra', 'Antumbra'};

%----------------------------------------
%---------- Subscribers
%----------------------------------------

Create OrbitView DefaultOrbitView;
GMAT DefaultOrbitView.SolverIterations = Current;
GMAT DefaultOrbitView.UpperLeft = [ -0.1082352941176471 0.004761904761904762 ];
GMAT DefaultOrbitView.Size = [ 0.9964705882352941 0.9630952380952381 ];
GMAT DefaultOrbitView.RelativeZOrder = 64;
GMAT DefaultOrbitView.Maximized = false;
GMAT DefaultOrbitView.Add = {CremeSat, Earth, Sun};
GMAT DefaultOrbitView.CoordinateSystem = EarthMJ2000Eq;
GMAT DefaultOrbitView.DrawObject = [ true true true ];
GMAT DefaultOrbitView.DataCollectFrequency = 1;
GMAT DefaultOrbitView.UpdatePlotFrequency = 50;
GMAT DefaultOrbitView.NumPointsToRedraw = 0;
GMAT DefaultOrbitView.ShowPlot = true;
GMAT DefaultOrbitView.MaxPlotPoints = 20000;
GMAT DefaultOrbitView.ShowLabels = true;
GMAT DefaultOrbitView.ViewPointReference = Earth;
GMAT DefaultOrbitView.ViewPointVector = [ 30000 0 0 ];
GMAT DefaultOrbitView.ViewDirection = Earth;
GMAT DefaultOrbitView.ViewScaleFactor = 1;
GMAT DefaultOrbitView.ViewUpCoordinateSystem = EarthMJ2000Eq;
GMAT DefaultOrbitView.ViewUpAxis = Z;
GMAT DefaultOrbitView.EclipticPlane = Off;
GMAT DefaultOrbitView.XYPlane = On;
GMAT DefaultOrbitView.WireFrame = Off;
GMAT DefaultOrbitView.Axes = On;
GMAT DefaultOrbitView.Grid = Off;
GMAT DefaultOrbitView.SunLine = On;
GMAT DefaultOrbitView.UseInitialView = On;
GMAT DefaultOrbitView.StarCount = 7000;
GMAT DefaultOrbitView.EnableStars = On;
GMAT DefaultOrbitView.EnableConstellations = On;

Create GroundTrackPlot DefaultGroundTrackPlot;
GMAT DefaultGroundTrackPlot.SolverIterations = Current;
GMAT DefaultGroundTrackPlot.UpperLeft = [ -0.06411764705882353 0.04880952380952381 ];
GMAT DefaultGroundTrackPlot.Size = [ 0.9964705882352941 0.9630952380952381 ];
GMAT DefaultGroundTrackPlot.RelativeZOrder = 66;
GMAT DefaultGroundTrackPlot.Maximized = false;
GMAT DefaultGroundTrackPlot.Add = {CremeSat, Toulouse};
GMAT DefaultGroundTrackPlot.DataCollectFrequency = 1;
GMAT DefaultGroundTrackPlot.UpdatePlotFrequency = 50;
GMAT DefaultGroundTrackPlot.NumPointsToRedraw = 0;
GMAT DefaultGroundTrackPlot.ShowPlot = true;
GMAT DefaultGroundTrackPlot.MaxPlotPoints = 20000;
GMAT DefaultGroundTrackPlot.CentralBody = Earth;
GMAT DefaultGroundTrackPlot.TextureMap = 'ModifiedBlueMarble.jpg';


%----------------------------------------
%---------- Mission Sequence
%----------------------------------------

BeginMissionSequence;
Propagate Propagator1(CremeSat) {CremeSat.ElapsedDays = 1};
