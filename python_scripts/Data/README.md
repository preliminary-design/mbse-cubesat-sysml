# How to compile the data section
This directory is organised in several input files, with the suggested name starting with `CONFIG_`.
In the following sections there is an highlight of the needed inputs.

You can change the filenames, except from `CONFIG_input.yaml`.

## CONFIG_input.yaml
This file contains:

- `PATHS`: the needed paths to the dependencies of the sw
    - `scilab_path`: you shoul indicate the _scilab_ file inside the `bin` folder. Example `INSTALLATION_PATH/scilab-2023.1.0/bin/scilab`
    - `GMAT_BIN_linux`: self explanatory, generally is in the `dependencies` folder. Example `./dependencies/GMAT_Application/GMAT/R2020a/bin/GmatConsole-R2020a`
    - `GMAT_BIN_linux_docker`: name of the docker image generated from the _dockerfile_ inside `./dependencies/GMAT_Application` folder. Example `gmat:gui`. It will be automatically called from the code.
    - `nss_adcs_dir`: directory to the _NSS ADCS_ module for ADCS faceability analysis. 
    - `input_dir`: self explanatory. Directory for the GMAT input file.
    - `output_dir`: self explanatory. Directory for all the outputs of the analysis.
    - `config_files`: Directory for all `CONFIG_` files.
- `SIMULATION`:
    - `report_name`: name of the desired output file.
    - `total_time`: total simulation time [days].
    - `dt`: simulation timestep [sec].
- `FILENAMES`: reference filenames for the `CONFIG_` files. Each file will be 
    - `orbit_config`: filename of the orbit and S/C flight dynamics `CONFIG_` file.
    - `modes_config`: filename of the S/C Operative Modes `CONFIG_` file.
    - `eps_config`: filename of the Electrical Power System `CONFIG_` file.
    - `ground_station_config`: filename of the ground station `CONFIG_` file.
    - `comm_sys_config`: filename of the S/C communication system `CONFIG_` file.
    - `comm_losses_config`: filename of the `CONFIG_` file for the definition of the losses during the downlink/uplink.
- `CONSTANTS`: self explanatory. Useful constants for analysis.