# Python scripts for pre-sizing


![Example of outputs](./figures/BatteryRemainingPower_3U.png)

## Authors
Jérôme Puech and Thibault Gateau

## Data

Put all data input files :
    - input.yaml : all inputs needed in yaml format
    - contactlocator from GMAT (automatically produced)  
    - eclipseLocator from GMAT (automatically produced)
    - data700km_bottom_test.cvs (protons energy over latitude and longitude at 700km)

## Output

Here are located all script outputs : 

    - Figures :
        - remaining power graph
        - data budget graph
    - All mission analysis markdown reports
    - Res_champ : 
        - output figures from the magnetic field analysis

## Scripts

### generalMissionAnalysis.py (Windows and Linux)


#### Requirements

Python3

'''
pip install MdUtils  
pip install pyyaml  
pip install yamlloader  
pip install sphinx
pip install sphinx_rtd_theme  
'''

GMAT installed
(see https://sourceforge.net/projects/gmat/)

Celestlab installed 
(see https://logiciels.cnes.fr/fr/content/celestlab)

and set their path correctly in input.yml file (see below)

#### Usage

'''
python generalMissionAnalysis.py 
'''

#### Doc

'''
python generalMissionAnalysis.py -h
'''

Compute nanosat battery consumption, data budget and link budget from gmat calculation.

Steps to use it : 

    - Set your inputs file (Data/input.yaml)
    - Do not forget to set the path to your scilab application and GMAT application in input.yaml
    - Run the script (Tip : In Spyder, go to Tools > Preferences > IPython console > graphics and set the backend to automatic to have a nice figure display, and restart spyder)
        - You can either define an SSO orbit or all keplerian parameters manually (use --SSO option or not)
        - If you want to change other simulation parameters, you have to change the GMAt file GMAT/CREME/CremeSat.script using GMAT
    - Go to the output and see the report / figures

### Magnetic_field_study/magnetic_field_celestlab_celestlab.py

Compute the angle between the sun direction and/or sun orthogonal direction and the magnetic field vector during mission.

#### Requirements

Python3

'''
pip install mplot3d
'''

#### Usage

Open the script and put your user inputs. 

'''
python magnetic_field_celestlab_celestlab.py
python magnetic_field_celestlab_celestlab.py --SSO
'''

### luplink.py 

#### Requirements

Python3

'''
pip install numpy
'''

Python2 for .ini to .json
(if you want to modify .ini file)

#### Usage

test: 
'''
python luplink.py
'''

Usage: To get the link budget at 300km: 
'''
import luplink
getLinkBugdet("Data/input.yaml",300,verbose=True)
'''

## docs

Open docs/_build/html/index.html to get the application documentation.
    
## Old Scripts

See readme in OldScripts folder. Be careful, this folder is not updated.


## TODO

- update battery_fig_path return 
- update gmat variable path (and rm windows style path, little bug on "..\..\eclipseLocator.txt seen as a file by unix ^^)
- finish gmat in docker
- renameing frun -> run
- rewrinting fdefineOrbitFromScilab
- rewriting fwriteGMATscript
- Do  not modify -> itermediary results (scurpts) sjould be also in output



 

