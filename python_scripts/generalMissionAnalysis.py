from functionsDatabase import *
from optparse import OptionParser

import os
import sys
from luplink import prettyMD,getLinkBugdet

import yaml
import yamlloader
from pint import UnitRegistry
import numpy as np
u = UnitRegistry()
from os import path
from collections import OrderedDict

import dependencies.gaphor_parser as gp

if __name__ == "__main__":
    #----------------------------------------
    #                 MAIN
    #----------------------------------------

    runGmat = True
    #runGmat = False
    runGaphorParser = True
    # runGaphorParser = False    

    defaultFile = './Data/CONFIG_input.yaml'
    
    parser = OptionParser()
    #parser.add_option("-f", "--file", dest="filename",
    #                  help="write report to FILE", metavar="FILE",default=defaultInitFile)
    parser.add_option("--SSO",
                    action="store_true", dest="bSSO", default=False,
                    help="define orbit giving SSO parameters")  
    
    parser.add_option("--gui",
                    action="store_true", dest="bGUI", default=False,
                    help="display GMAT gui and wait for the user to close it")
    parser.add_option("-f", "--file", dest="filename",
                      help="input FILE, default is <"+str(defaultFile)+">", metavar="FILE",default=defaultFile) 
    '''
    parser.add_option("-i", "--initialEpoch", dest="epoch",
                    help="initial epoch (default: \"2023,01,01,00,00,00\")", metavar="FILE",default="2023,01,01,00,00,00")
    '''

    (options, args) = parser.parse_args()

    # ---------- DATA LOADING ---------
    
    if runGaphorParser:
        # ------ PARSE GAPHOR MODEL
        modelFile = '/home/glucc/tesi/intern-2023-giacomo-luccisano-preliminarydesign/models/gaphor/heo_model/heo_model.gaphor'
        modelOutDir = './gaphor_parser_out/'

        print("Loading .gaphor model from " + modelFile)

        config_for_report = gp.parseModel(modelFile, modelOutDir)
        configTmp = OrderedDict(config_for_report)

        for comp in config_for_report:
            configTmp[comp] = gp.deleteUnits(config_for_report[comp])

        config = readConfigFromParser(configTmp)
        configTmp = {}
        stream = open(options.filename, 'r')
        configTmp = yaml.load(stream, Loader = yamlloader.ordereddict.CSafeLoader)
        configTmp = delUnits(configTmp)
        config.update(configTmp)
        config_for_report.update(configTmp)
        stream.close()

        print(".gaphor model parsed!")
    else:
        #------ READ FROM INPUT FILES
        print("Loading inputs from " + defaultFile)
        
        stream = open(options.filename, 'r')
        config = yaml.load(stream, Loader = yamlloader.ordereddict.CSafeLoader)
        stream.close()
   
        for elem in config["FILENAMES"].values():
            config = readConfigFile(config, elem)

        config_for_report = sepUnits(config)
        config = delUnits(config)

        print("Inputs loaded!")

    print("Starting simulation...")
    print("----------")

    #computeOrbitography(options)
    #-------ORBITOGRAPHY------------
    # Users can either specify the orbital parameters by defining an SSO (altitude, epoch and mltan)
    # or the 6 keplerian parameters in input.yaml

    bSSO = options.bSSO
    bGUI = options.bGUI
    #epoch = options.epoch
    if bSSO:
        config_for_report = defineOrbitFromScilab(config,config_for_report)
    else:
        print("Taking orbital parameters from input file.")

    print("----------")
    if not path.isdir(config["paths"]["output_dir"]):
        os.makedirs(config["paths"]["output_dir"])

    if not path.isdir(config["paths"]["output_dir"]+"Figures"):
        os.makedirs(config["paths"]["output_dir"]+"Figures")

    #--------GMAT simulation--------
    print("Start GMAT calculation ...")
    #Write new script from default script (DefaultSat.script --> MissionFromPython.script)
    epoch = config["ORBIT"]["epoch"]
    initial_epoch = getGMATepochformat(epoch) 
    print(initial_epoch)
    print("duration: "+str(config['SIMULATION']['total_time']))
    writeGMATscript(config)

    #Open and run GMAT simulation with new orbits parameters
    if os.name == 'posix':     
        runMissionFromPythonInDocker(config,runGmat)      
    else:
        runMissionFromPython(config['paths']['GMAT_BIN'], bGUI)
        
    print("GMAT computation done \n ----------")

    #------------COMPUTE DATA----------------

    #Compute contact times
    contact_data = detectEvents(config['paths']['output_dir']+"ContactLocator1.txt", initial_epoch)
    #Compute eclipse times
    eclipse_data = detectEvents(config['paths']['output_dir']+"EclipseLocator1.txt", initial_epoch)
    
    #Detect if there are no events
    if detectNoEvents(eclipse_data, contact_data): 
        print("No contact or eclipse time during this period, always in mode charge")
        print("Breaking the simulation as no useful event found...")
        exit()
    
    
    visibility_data = findVisibility(config['paths']['output_dir']+'ContactLocator1.txt', contact_data[3], config['SIMULATION']['total_time'])
    max_eclipse_time = findMaxEclipse(config['paths']['output_dir']+'EclipseLocator1.txt', eclipse_data[3])
    print("Max Eclipse time [s]: "+str(max_eclipse_time))
    #Compute general time data
    xlim = int(config['SIMULATION']['total_time']*86400) # Convert days to sec
    seconds = computeGeneralTimeVector(xlim) 

    print("Compute plots ...")
    #Compute satellite modes and remaining power during simulation
    
    TotBatPower = config['POWER_BUDGET']['TotBatPower']
    SolarPower = config['POWER_BUDGET']['SolarPower']
    
    print("compute Battery Consumption...")
    
    [mode, remaining_power] = computeBatteryConsumption(xlim, eclipse_data, contact_data, TotBatPower, SolarPower, config)
    
    print("plot Battery Consumption...")    
    battery_fig_path = plotBatteryConsumption(config,eclipse_data, contact_data, seconds, xlim, mode, remaining_power, TotBatPower)

    #Compute data budget during simulation
    try:
        # TC_debit_nominal = [float(config["MODE_DEFAULT"]["TC_debit_nominal"])]
        # TM_debit_nominal = [float(config["MODE_DEFAULT"]["TM_debit_nominal"])]
        TC_debit_nominal = []
        TM_debit_nominal = []
        for i in range(int(config["OPERATING_MODES"]["N_of_modes"])):
            TC_debit_nominal.append(float(config["OPERATING_MODES"]["MODE_"+str(int(i))]["TCRate"]))
            TM_debit_nominal.append(float(config["OPERATING_MODES"]["MODE_"+str(int(i))]["TMRate"]))

        [contactTC, contactTM] = computeDataQuantities(contact_data, TC_debit_nominal, TM_debit_nominal, mode, config)  # need to pass also the modes array
        data_fig_path = plotDataBudget(config,contact_data, contactTC, contactTM)
    except Exception as e:
        os.remove(battery_fig_path)
        printException(e)
        sys.exit()

    print("Done ! \n ----------")

    #Compute link budget
    #report = getLinkBugdet("Data/luplink_Creme_S.json",float(SMA) - 6378.136 , False)
    try:
        
        altitude_max = getAltitudeMax(config)
        report = getLinkBugdet(options.filename, config, altitude_max , False)
        stringSyntheseLinkBudget = '|Category|Data|Value|Unit|\n'
        stringSyntheseLinkBudget += '| :---: | :---: | :---: | :---: |\n'
        stringSyntheseLinkBudget += prettyMD(report)
    except Exception as e:
        os.remove(battery_fig_path)
        os.remove(data_fig_path)
        printException(e)
        sys.exit()
    
    power_output_for_report = computeLinkBudget(config)
    

    #------------SAVE REPORT----------------

    #Save general report
    print("----------")
    print("Saving report ... ")
    
    shiftName = len(config['paths']['output_dir']) 
    
    try :
        saveReport(config,config_for_report, battery_fig_path[shiftName:], data_fig_path[shiftName:], stringSyntheseLinkBudget, visibility_data, max_eclipse_time, power_output_for_report)

    except Exception as e:
        os.remove(battery_fig_path)
        os.remove(data_fig_path)
        printException(e)
        sys.exit()

    print("Done !")

    print("----------")
    print("Simulation finished !")