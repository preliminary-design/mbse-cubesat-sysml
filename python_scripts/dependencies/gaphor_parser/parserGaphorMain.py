""" Copyright 2024 Giacomo Luccisano.

This file is part of Gaphor_UML_CubeSat_Stereotypes_Parser.

Gaphor_UML_CubeSat_Stereotypes_Parser is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gaphor_UML_CubeSat_Stereotypes_Parser is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Gaphor_UML_CubeSat_Stereotypes_Parser.  If not, see <http://www.gnu.org/licenses/>.
"""

import xml.etree.ElementTree as ET
import os
import yaml
from .parserFunctionsDB import *
from .parserGraphWriter import *
from copy import deepcopy
import argparse


def parseModel(modelFile: str, outPath: str) -> dict[str, dict[str, any]]:
    """ 
    Reads a modelFile.gaphor file and translates into a .yaml database.

    Input:
    - modelFile: path and filename of the .gaphor file
    - outPath: output path for database

    Output:
    - none
    """

    tree = ET.parse(modelFile)
    root = tree.getroot()

    ns = {'gNS':'http://gaphor.sourceforge.net/model'}

    os.makedirs(outPath, exist_ok=True)
    os.makedirs(outPath+'components', exist_ok=True)

    # get and print all requirements

    reqList = getRequirements(ns, root)
    printReqList(reqList, outPath)

    # get all components with stereotypes applied

    stereotypes = getStereotypes(ns, root)

    components = {}

    for st in stereotypes:    
        components[st['name']] = getComponents(ns, root, st)

    # get composite association between systems and their components

    for sys in components['SystemComp']:
        subEl = getSubElements(ns, root, components['SystemComp'][sys])
        if subEl != {}:
            components['SystemComp'][sys]['SubElements'] = subEl

    for sys in components['SystemMain']:
        subEl = getSubElements(ns, root, components['SystemMain'][sys])
        if subEl != {}:
            components['SystemMain'][sys]['SubElements'] = subEl

    # print components .yaml files

    bufferEPS = {}
    bufferCommSys = {}

    for comp in components:
        if comp == 'SolarArray' or comp == 'Battery':
            bufferEPS[comp] = components[comp]
            with open(outPath+'components/'+'EPS.yaml', 'w') as file:
                yaml.dump(bufferEPS, file, default_flow_style=False, sort_keys=True)
        elif comp == 'Transmitter' or comp == 'Receiver':
            bufferCommSys[comp] = components[comp]
            with open(outPath+'components/'+'CommSys.yaml', 'w') as file:
                yaml.dump(bufferCommSys, file, default_flow_style=False, sort_keys=True)
        else:
            with open(outPath+'components/'+comp+'.yaml', 'w') as file:
                yaml.dump(components[comp], file, default_flow_style=False, sort_keys=True)

    # delete units and convert values

    cleanComps = deepcopy(components)
    for comp in components:
        cleanComps[comp] = deleteUnits(cleanComps[comp])
    
    # inherit masses and power consumptions from SysComp to SysMain
    if 'SystemMain' in cleanComps.keys() and 'SystemComp' in cleanComps.keys():
        for sysM in cleanComps['SystemMain']:
            if cleanComps['SystemMain'][sysM]['computeCons']:
                for prop in ['powerConsOn', 'powerConsStb', 'powerConsPeak']:
                    cleanComps = inheritMainProp(cleanComps, sysM, prop)
            elif cleanComps['SystemMain'][sysM]['computeMass']:
                cleanComps = inheritMainProp(cleanComps, sysM, 'mass')    

    # edit model with new data
    printModelFile(modelFile, ns, root, tree, stereotypes, cleanComps)

    # print relationship graphs
    printGraph(cleanComps, outPath)

    return components

if __name__ == "__main__":
    
    modelFileDef = './tutorial/examples/heo_model.gaphor'
    outPathDef = './gaphor_parser_out/'

    parser = argparse.ArgumentParser(description='Parse a Gaphor model file.')
    parser.add_argument('-i', '--in', dest='modelFile', type=str, 
                        nargs='?', default=modelFileDef, 
                        help='Path to the Gaphor model file. Default is ' + modelFileDef)
    parser.add_argument('-o', '--out', dest = 'outPath', type=str, 
                        nargs='?', default=outPathDef, 
                        help='Output path for the parser. Default is ' + outPathDef)

    args = parser.parse_args()

    comp = parseModel(modelFile=args.modelFile, outPath=args.outPath)
