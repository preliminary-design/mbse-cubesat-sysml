""" Copyright 2024 Giacomo Luccisano.

This file is part of Gaphor_UML_CubeSat_Stereotypes_Parser.

Gaphor_UML_CubeSat_Stereotypes_Parser is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gaphor_UML_CubeSat_Stereotypes_Parser is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Gaphor_UML_CubeSat_Stereotypes_Parser.  If not, see <http://www.gnu.org/licenses/>.
"""

import graphviz

def printCompSubGraph(compName: str, components: dict[str, dict[str, any]], 
                      sysComp: graphviz.graphs.Digraph, existingSys: list[str]) -> list[str]:
    """ 
    Generates subgraph for assigned compName and links masses with TRL margins.
     
    Input:
    - compName: str with component key to check.
    - components: dict with components list.
    - sysComp: dot variable with ref to subgraph.
    - existingSys: list that contains all generated components properties.
    - excluded: properties to not be printed.
    
    Output:
    - existingSys: updated list of printed components."""

    excluded = ['compID', 'compName', 'manifacturer', 'SubElements', 'id', 'computeMass', 'computeCons']

    if compName in components.keys():
        for comp in components[compName]:
                with sysComp.subgraph(name='cluster_'+comp) as c:
                    c.attr(label=comp)
                    for key in components[compName][comp]:
                        if key not in excluded:
                            c.node(comp+key, key)
                            
                            # edges between margins and masses
                            if key == 'margin':
                                if comp+'mass' in existingSys:
                                    c.edge(comp+key, comp+'mass')
                                elif comp+'powerConsStb' in existingSys:
                                    c.edge(comp+key, comp+'powerConsStb')
                                elif comp+'powerConsOn' in existingSys:
                                    c.edge(comp+key, comp+'powerConsOn')
                                elif comp+'powerConsPeak' in existingSys:
                                    c.edge(comp+key, comp+'powerConsPeak')
                            elif key == 'mass' and comp+'margin' in existingSys:
                                c.edge(comp+'margin', comp+key)
                            elif key == 'powerConsStb' and comp+'margin' in existingSys:
                                c.edge(comp+'margin', comp+key)
                            elif key == 'powerConsOn' and comp+'margin' in existingSys:
                                c.edge(comp+'margin', comp+key)
                            elif key == 'powerConsPeak' and comp+'margin' in existingSys:
                                c.edge(comp+'margin', comp+key)

                            existingSys.append(comp+key)

    return existingSys

# ======================================================================================

def printOMSubGraph(omName: str, components: dict[str, dict[str, any]], 
                      oms: graphviz.graphs.Digraph, existingOMs: list[str]) -> list[str]:
    """ 
    Generates subgraph for assigned omName.
     
    Input:
    - omName: str with Operating Mode key to check.
    - components: dict with OMs list.
    - oms: dot variable with ref to subgraph.
    - existingOMs: list that contains all generated OMs properties.
    
    Output:
    - existingOMs: updated list of OMs."""

    excluded = ['priority', 'id', 'modeID', 'modeName', 'postProColor']
    criteria = ['isEclipse', 'isInInterval', 'isInRange', 'isOnTarget', 'isOnZone']
    conditions = ['intervalStart', 'intervalEnd', 'rangeStart', 'rangeEnd',
                'zoneLatStart', 'zoneLatEnd', 'zoneLonStart', 'zoneLonEnd']
    
    excluded.extend(criteria)
    excluded.extend(conditions)

    with oms.subgraph(name='cluster_'+omName) as c:
        c.attr(label=omName)
        for key in components['OperatingMode'][omName]:
            if key not in excluded:
                c.node(omName+key, key)

            elif key in criteria and components['OperatingMode'][omName][key]:
                
                c.node(omName+key, key)

                if key == 'isInInterval':
                    c.node(omName+'intervalStart', 'intervalStart')
                    c.node(omName+'intervalEnd', 'intervalEnd')
                    c.edge(omName+key, omName+'intervalStart')
                    c.edge(omName+key, omName+'intervalEnd')    
                elif key == 'isInRange':
                    c.node(omName+'rangeStart', 'rangeStart')
                    c.node(omName+'rangeEnd', 'rangeEnd')
                    c.edge(omName+key, omName+'rangeStart')
                    c.edge(omName+key, omName+'rangeEnd') 
                elif key == 'isOnZone':
                    c.node(omName+'zoneLatStart', 'zoneLatStart')
                    c.node(omName+'zoneLatEnd', 'zoneLatEnd')
                    c.node(omName+'zoneLonStart', 'zoneLonStart')
                    c.node(omName+'zoneLonEnd', 'zoneLonEnd')
                    c.edge(omName+key, omName+'zoneLatStart')
                    c.edge(omName+key, omName+'zoneLatEnd') 
                    c.edge(omName+key, omName+'zoneLonStart')
                    c.edge(omName+key, omName+'zoneLonEnd') 

            existingOMs.append(omName+key)

    return existingOMs

# ======================================================================================

def printEdgeSMtoSC(g: graphviz.graphs.Digraph, value: str, compName: str, sysName: str, 
                    existingSys: list[str], existingComps: list[str]) -> None:
    """  
    Generate an edge between compNamevalue and sysNamevalue nodes  in a g graph.
    
    Input:
    - g: graphviz.graphs.Digraph pointer with ref to the graph.
    - value: str with common property to link.
    - compName: str with parent component name.
    - sysName: str with parent system name.
    - existingSys: list with existing systems nodes.
    - existingComps: list with existing components nodes.

    Output:
    - none.
    """

    if compName+value in existingComps and sysName+value in existingSys:
                    g.edge(compName+value, sysName+value)

# ======================================================================================

def printEdgeSCtoSC(g: graphviz.graphs.Digraph, value: str, compName1: str, 
                    compName2: str, existingComps: list[str]) -> None:
    """  
    Generate an edge between compName1value and compName2value nodes  in a g graph.
    
    Input:
    - g: graphviz.graphs.Digraph pointer with ref to the graph.
    - value: str with common property to link.
    - compName1: str with tail parent component name.
    - compName2: str with head parent component name.
    - existingComps: list with existing components nodes.

    Output:
    - none.
    """

    if compName1+value in existingComps and compName2+value in existingComps:
                    g.edge(compName1+value, compName2+value)

# ======================================================================================

def printEdgeSMtoOM(g: graphviz.graphs.Digraph, sysMain: str, om: str, components: dict[str, dict[str, any]]) -> None:
    """  
    Generate an edge between SysMain power consumption and relative OM power consumption nodes in a g graph.
    
    Input:
    - g: graphviz.graphs.Digraph pointer with ref to the graph.
    - sysMain: str with tail parent component name.
    - om: str with head parent OM name.
    - components: list with existing components.

    Output:
    - none.
    """

    if 'modeIsStb' in components['SystemMain'][sysMain].keys():
        for name in components['SystemMain'][sysMain]['modeIsStb']:
            if name == components['OperatingMode'][om]['modeName']:
                g.edge(sysMain+'modeIsStb', om+'powerCons')
                g.edge(sysMain+'powerConsStb', om+'powerCons')

    if 'modeIsOn' in components['SystemMain'][sysMain].keys():
        for name in components['SystemMain'][sysMain]['modeIsOn']:
            if name == components['OperatingMode'][om]['modeName']:
                g.edge(sysMain+'modeIsOn', om+'powerCons')
                g.edge(sysMain+'powerConsOn', om+'powerCons')

    if 'modeIsPeak' in components['SystemMain'][sysMain].keys():
        for name in components['SystemMain'][sysMain]['modeIsPeak']:
            if name == components['OperatingMode'][om]['modeName']:
                g.edge(sysMain+'modeIsPeak', om+'powerCons')
                g.edge(sysMain+'powerConsPeak', om+'powerCons')

# ======================================================================================

def printGraph(components: dict[str, dict[str, any]], outPath: str) -> None:
    """ 
    Generates a .dot graph which highlights relationships between components.

    Input:
    - components: dict with components list.
    - outPath: string with desired output file path.
    
    Output:
    - none
    """

    g = graphviz.Digraph('relationGraph')

    # graph attributes
    g.attr('node', shape='box')

    # print all existing Components
    existingComps = []
    compList = ['SystemComp', 'Battery', 'SolarArray', 'Transmitter', 'Receiver']
    
    if 'SystemComp' in components.keys():
        with g.subgraph(name='cluster_sysComp') as sysComp:
            sysComp.attr(label='Systems Components')
            sysComp.attr('node', color = 'orange', style = 'filled')

            for compName in compList:
                existingComps = printCompSubGraph(compName, components, sysComp, existingComps)

    # print all existing systems 
    existingSys = []
    if 'SystemMain' in components.keys():
        with g.subgraph(name='cluster_sysMain') as sysMain:
            sysMain.attr(label='Main Systems')
            sysMain.attr('node', color = 'lightblue', style = 'filled')

            existingSys = printCompSubGraph('SystemMain', components, sysMain, existingSys)
            
    # print edges between components and main systems
    values = ['mass', 'powerConsOn', 'powerConsStb', 'powerConsPeak']
    
    if 'SystemMain' in components.keys():
        for comp in components['SystemMain']:
            if 'SubElements' in components['SystemMain'][comp].keys():
                for key in components['SystemMain'][comp]['SubElements']:
                    for value in values:
                        printEdgeSMtoSC(g, value, key, comp, existingSys, existingComps)

    # print edges between components and subcomponents 
    if 'SystemComp' in components.keys():
        for comp in components['SystemComp']:
            if 'SubElements' in components['SystemComp'][comp].keys():
                for key in components['SystemComp'][comp]['SubElements']:
                    for value in values:
                        printEdgeSCtoSC(g, value, key, comp, existingComps)

    # print operating modes
    if 'OperatingMode' in components.keys():
        existingOMs = []

        with g.subgraph(name='cluster_OM') as oms:
            oms.attr(label='Operating Modes')
            oms.attr('node', color = 'lightgreen', style = 'filled')

            for omName in components['OperatingMode']:
                existingOMs = printOMSubGraph(omName, components, oms, existingOMs)

    # print edges between OMs and SysMain
    if 'SystemMain' in components.keys() and 'OperatingMode' in components.keys():
        for sysMain in components['SystemMain']:
            for om in components['OperatingMode']:
                printEdgeSMtoOM(g, sysMain, om, components)



    g.render(directory=outPath).replace('\\', '/')
