



# GMAT with Docker: quickstart

```
docker run -it --rm -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$DISPLAY  gitlab-registry.isae-supaero.fr/creme/creme-systeme/gmat:dev
```

# GMAT with Docker: generate docker locally and run it


## old
```
cd ~/git/creme/creme-systeme/0_Simulation/GMAT_Application
docker build -t gmat:dev .
docker run -it --rm -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$DISPLAY  gmat:dev
```


## console
docker run -it --rm -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$DISPLAY  gmat:gui bash


## with GUI: 
```
cd ~/git/creme/creme-systeme/0_Simulation/GMAT_Application
docker build -t gmat:gui .
docker run -it --rm -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$DISPLAY  gmat:gui bash
/app/R2020a/bin/GMAT_Beta

```

## with GUI and bind: 

```
docker run -it --mount type=bind,source=/home/tgateau/git/creme/creme-systeme/0_Simulation/Python/Output_Lite,target=/app/Output_Lite  --rm -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$DISPLAY  gmat:gui bash
/app/R2020a/bin/GMAT_Beta 
#bug in gmat

```

## with console and bind and root: 
docker run -it  --mount type=bind,source=/home/tgateau/git/creme/creme-systeme/0_Simulation/Python/Output_Lite,target=/app/Output_Lite --rm  gmat:gui bash
cd Output_Lite
/app/R2020a/bin/GmatConsole --run MissionFromPython.script



#with bind
docker container run -it --mount type=bind,source=/home/tgateau/git/creme/creme-systeme/0_Simulation/Python/Output_Lite,target=/app/Output_Lite  gmat:dev /bin/bash
cd Output_Lite
## in console
cd /app/R2020a/bin/
./GmatConsole  --run /app/Output_Lite/MissionFromPython.script





#docker debug
docker run -it --rm -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$DISPLAY  -u root  gmat:gui bash #display with no work with '-u root'
docker run -it --rm -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$DISPLAY fr3nd/xeyes


# GMAT with Docker in console mode: generate docker locally and run it

```
cd ~/git/creme/creme-systeme/0_Simulation/GMAT_Application
docker build -t gmatConsole:dev .
docker run -it --rm -v  gmatConsole:dev
```


# push docker container on gitlab

```
cd ~/git/creme/creme-systeme/0_Simulation/GMAT_Application
docker login gitlab-registry.isae-supaero.fr
docker build -t gitlab-registry.isae-supaero.fr/creme/creme-systeme/gmat:dev .
docker push gitlab-registry.isae-supaero.fr/creme/creme-systeme/gmat:dev
```

# Troubleshooting

## test if X server with docker is working

```
cd ~/git/creme/creme-systeme/0_Simulation/GMAT_Application/xeyes
docker build -t xeyes:dev .
docker run -it --rm -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$DISPLAY  xeyes:dev
```

## docker troubles


The docker exec command is probably what you are looking for; this will let you run arbitrary commands inside an existing container. For example:
docker exec -it <mycontainer> bash

cd /home/tgateau/git/creme/creme-systeme/0_Simulation/

docker container run -it  gmat:dev /bin/bash
docker container run -it --mount type=bind,source=/home/tgateau/git/creme/creme-systeme/0_Simulation/GMAT/Cubesat_DO_NOT_MODIFY,target=/app/Cubesat_DO_NOT_MODIFY  gmat:dev /bin/bash
















