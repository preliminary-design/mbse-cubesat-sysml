import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from matplotlib.backends.backend_pdf import PdfPages
from datetime import datetime
from sys import exit
import sys
import os
from mdutils.mdutils import MdUtils
import traceback
import copy
from luplink import getSlantRange
import yaml
import yamlloader
import dependencies.gaphor_parser as gp

onlyTest = False

#-----------------------------------------------------------------------------

def readConfigFromParser(config: dict[str, dict[str, any]]) -> dict[str, dict[str, any]]:
    """ 
    Adapts config structure for generalMissionAnalysis.py use.

    Input:
    - config: dict with structure from gaphor parser.

    Output:
    - cnf: dict with structure adapted for mission analysis tool use.
    """

    cnf = {'POWER_BUDGET':{}, 'SPACECRAFT_TRANSMITTER':{}, 'DOWNLINK':{}, 'SPACECRAFT_RECEIVER':{},
           'PROPAGATION_LOSSES': {}, 'ORBIT': {}, 'SPACECRAFT_FLIGHT_DYNAMICS': {'SC_altitude': 0},
           'GROUND_STATION_TRANSMITTER': {}, 'GROUND_STATION_RECEIVER': {}, 'GROUNDSTATION_LOCATION': {}, 
           'UPLINK': {}, 'OPERATING_MODES':{}}

    # if 'SystemMain' in config.keys() and 'SystemComp' in config.keys():
    #     for sysM in config['SystemMain']:
    #         if config['SystemMain'][sysM]['computeCons']:
    #             for prop in ['powerConsOn', 'powerConsStb', 'powerConsPeak']:
    #                 config = gp.inheritProp(config, sysM, prop)
    #         elif config['SystemMain'][sysM]['computeMass']:
    #             config = gp.inheritProp(config, sysM, 'mass')

    for key, value in config.items():
        if key == 'Transmitter':
            for k,v in value.items():
                cnf['SPACECRAFT_TRANSMITTER']['SC_power_tx'] = v['power']
                cnf['SPACECRAFT_TRANSMITTER']['SC_ant_gain_tx'] = v['gain']
                cnf['SPACECRAFT_TRANSMITTER']['SC_loss_cable_tx'] = v['lossCable']
                cnf['SPACECRAFT_TRANSMITTER']['SC_loss_connector_tx'] = v['lossConnector']
                cnf['SPACECRAFT_TRANSMITTER']['SC_loss_feeder_tx'] = v['lossFeeder']
                cnf['SPACECRAFT_TRANSMITTER']['SC_misc'] = v['lossMisc']
                cnf['SPACECRAFT_TRANSMITTER']['SC_loss_point_tx'] = v['lossPoint']
                cnf['DOWNLINK']['frequency'] = v['freq']
                cnf['DOWNLINK']['data_rate'] = v['dataRate']
                cnf['DOWNLINK']['required_BER_from_Modulation'] = v['reqBER']
                cnf['DOWNLINK']['modulation'] = v['modulation']
                cnf['DOWNLINK']['system_margin'] = v['sysMargin']
        elif key == 'Receiver':
            for k,v in value.items():
                cnf['SPACECRAFT_RECEIVER']['SC_ant_gain_rx'] = v['gain']
                cnf['SPACECRAFT_RECEIVER']['SC_eta_rx'] = v['etaRx']
                cnf['SPACECRAFT_RECEIVER']['SC_loss_point_rx'] = v['lossPoint']
                cnf['SPACECRAFT_RECEIVER']['SC_loss_cable_rx'] = v['lossCable']
                cnf['SPACECRAFT_RECEIVER']['SC_loss_connector_rx'] = v['lossConnector']
                cnf['SPACECRAFT_RECEIVER']['SC_LNA_gain'] = v['lnaGain']
                cnf['SPACECRAFT_RECEIVER']['SC_T_rx'] = v['noiseTemp']
        elif key == 'Battery':
            for k,v in value.items():
                cnf['POWER_BUDGET']['TotBatPower'] = v['capacity']
        elif key == 'SolarArray':
            for k,v in value.items():
                cnf['POWER_BUDGET']['SolarPower'] = v['producedPower']
        elif key == 'PropagationLosses':
            for k,v in value.items():
                cnf['PROPAGATION_LOSSES']['loss_pol'] = v['lossPol']
                cnf['PROPAGATION_LOSSES']['loss_atm'] = v['lossAtm']
                cnf['PROPAGATION_LOSSES']['loss_scin'] = v['lossScin']
                cnf['PROPAGATION_LOSSES']['loss_rain'] = v['lossRain']
                cnf['PROPAGATION_LOSSES']['loss_cloud'] = v['lossCloud']
                cnf['PROPAGATION_LOSSES']['loss_si'] = v['lossSnowIce']
                cnf['PROPAGATION_LOSSES']['loss_misc'] = v['lossMisc']
        elif key == 'Orbit':
            for k,v in value.items():
                cnf['ORBIT']['epoch'] = v['startEpoch']
                cnf['ORBIT']['SMA'] = v['SMA']
                cnf['ORBIT']['ECC'] = v['ECC']
                cnf['ORBIT']['INC'] = v['INC']
                cnf['ORBIT']['RAAN'] = v['RAAN']
                cnf['ORBIT']['AOP'] = v['AOP']
                cnf['ORBIT']['TA'] = v['startTA']
        elif key == 'GroundStation':
            for k,v in value.items():
                cnf['GROUNDSTATION_LOCATION']['GS_altitude'] = v['altitude']
                cnf['GROUNDSTATION_LOCATION']['GS_minElevation'] = v['minElevation']
                cnf['GROUNDSTATION_LOCATION']['GS_latitude'] = v['Lat']
                cnf['GROUNDSTATION_LOCATION']['GS_longitude'] = v['Lon']
                cnf['GROUND_STATION_TRANSMITTER']['GS_power_tx'] = v['powerTx']
                cnf['GROUND_STATION_TRANSMITTER']['GS_line_loss_tx'] = v['lineLossTx']
                cnf['GROUND_STATION_TRANSMITTER']['GS_loss_connector_tx'] = v['lossConnectorTx']
                cnf['GROUND_STATION_TRANSMITTER']['GS_ant_gain_tx'] = v['gainTx']
                cnf['GROUND_STATION_TRANSMITTER']['GS_loss_point_rx'] = v['lossPointRx']
                cnf['GROUND_STATION_RECEIVER']['eta_rx'] = v['etaRx']
                cnf['GROUND_STATION_RECEIVER']['antennaDiameter'] = v['diameterRx']
                cnf['GROUND_STATION_RECEIVER']['GS_LNA_gain'] = v['LNAGain']
                cnf['GROUND_STATION_RECEIVER']['GS_T_rx'] = v['noiseTempRx']
                cnf['GROUND_STATION_RECEIVER']['GS_loss_point_rx'] = v['lossPointRx']
                cnf['GROUND_STATION_RECEIVER']['GS_loss_cable_rx'] = v['lossCableRx']
                cnf['GROUND_STATION_RECEIVER']['GS_loss_cable_D_rx'] = v['lossCableRx']
                cnf['GROUND_STATION_RECEIVER']['GS_loss_connector_rx'] = v['lossConnectorRx']
                cnf['UPLINK']['frequency'] = v['freq']
                cnf['UPLINK']['data_rate'] = v['dataRateTx']
                cnf['UPLINK']['required_BER_from_Modulation'] = v['reqBER']
                cnf['UPLINK']['modulation'] = v['modulation']
                cnf['UPLINK']['system_margin'] = v['sysMargin']
        elif key == 'OperatingMode':
            count = 0
            for k,v in value.items():
                name = v['modeName']
                for sysM in config['SystemMain']:
                    try:
                        if name in config['SystemMain'][sysM]['modeIsOn']:
                            v['powerCons'] += config['SystemMain'][sysM]['powerConsOn']
                        elif name in config['SystemMain'][sysM]['modeIsStb']:
                            v['powerCons'] += config['SystemMain'][sysM]['powerConsStb']
                        elif name in config['SystemMain'][sysM]['modeIsPeak']:
                            v['powerCons'] += config['SystemMain'][sysM]['powerConsPeak']  
                    except KeyError:
                        None

                cnf['OPERATING_MODES']['MODE_'+str(int(count))] = v
                count += 1
            cnf['OPERATING_MODES']['N_of_modes'] = count 

    return cnf

#-----------------------------------------------------------------------------

def readConfigFile(config, filename):
    '''Reads the CONFIG_file and adds that to the config.

    Args:
        config (orderedDict)
        filename (string): reference filename 

    Returns: 
       config updated with che content of filename
    
    '''

    print("Reading " + filename + ".yaml")

    stream = open(config["paths"]["config_files"] + filename +'.yaml')
    tmp_config = yaml.load(stream, Loader = yamlloader.ordereddict.CSafeLoader)
    stream.close()
    config.update(tmp_config) 

    return config

#-----------------------------------------------------------------------------

def defineOrbitFromScilab(config,config_for_report):
    '''Ask user to define an SSO orbit by giving the altitude[km], the initial epoch and the mean local time
    at ascending node. Return the orbit keplerian parameters. This function uses the SCILAB application to 
    process the orbit parameters.

    Args:
        epoch (str): Initial epoch (format : 'YYYY,MM,DD,hh,mm,ss')
        scilab_path (str): path to scilab application


    Returns: 
       list: A list containing all six keplerian parameters [epoch, SMA[km], ECC, INC[deg], RAAN[deg], AOP[deg], TA[deg]]
    
    '''
    
    scilab_path = config['paths']['scilab_path']
    epoch = config["ORBIT"]["epoch"]
    SMA = str(config["ORBIT"]["SMA"])
    ECC = str(config["ORBIT"]["ECC"])
    INC = str(config["ORBIT"]["INC"])
    RAAN = str(config["ORBIT"]["RAAN"])
    AOP = str(config["ORBIT"]["AOP"])
    TA = str(config["ORBIT"]["TA"])   
    # Using a scilab script and the C
    
    #celestlab library to compute SSO paramaters
    #------SCILAB-------
    #Running scilab script to compute orbit parameters
    print("Give SSO parameters :")
    os_res = os.system('"' + scilab_path + '" -f ../SCILAB/SSO.sce')
    if os_res != 0:
        exit('ERROR : Verify the path to your Scilab application in the input file')
        
    #Get orbit parameters from scilab outputs
    try:
        scilab_input = open('../SCILAB/Output/Output.txt','r')
        scilab_lines = scilab_input.readlines()
        scilab_input.close()
    except Exception as e:
        printException(e)
        sys.exit()

    #Delete \n into scilab lines
    for i in range(len(scilab_lines)):
        scilab_lines[i] = scilab_lines[i].replace("\n","")
    
    epoch = scilab_lines[6] + ',' + scilab_lines[7] + ',' + scilab_lines[8] + ',' + scilab_lines[9] + ',' + scilab_lines[10] + ',' + scilab_lines[11]
    SMA = scilab_lines[0]
    ECC = scilab_lines[1]
    INC = scilab_lines[2]
    RAAN = scilab_lines[3]
    AOP = scilab_lines[4]
    TA = scilab_lines[5]

    print("epoch : " + epoch)
    print("SMA [km] : " + SMA)
    print("eccentricite [/] : " + ECC)
    print("inclinaison [deg] : " + INC)
    print("RAAN [deg] : " + RAAN)
    print("AOP [deg] : " + AOP)
    print("TA [deg] : " + TA)
    print("SCILAB computation done")
    print("Option SSO detected : initial orbital parameters will be overwritten.")
    scilab_path = config['paths']['scilab_path']
    config_for_report = defineOrbitFromScilab(config, scilab_path,config_for_report)
    config_for_report["ORBIT"]["epoch"][0] = epoch
    config_for_report["ORBIT"]["SMA"][0] = SMA
    config_for_report["ORBIT"]["ECC"][0] = ECC
    config_for_report["ORBIT"]["INC"][0] = INC
    config_for_report["ORBIT"]["RAAN"][0] = RAAN
    config_for_report["ORBIT"]["AOP"][0] = AOP
    config_for_report["ORBIT"]["TA"][0] = TA

    return config_for_report

#-----------------------------------------------------------------------------

def detectEvents(dataFilePath, initial_epoch):
    '''Detect events from a GMAT locator report (eclipses or contacts).
    Locator reports are .txt files automatically generated by GMAT.
    Please refer to the DefaultSat.script file to see how they are configured in the GMAT app. 

    Args: 
        dataFilePath (str): Path to a GMAT event text file report (contactLocator or eclipseLocator report)
        initial_epoch (str): Initial GMAT simulation epoch for the considered file (format : DD Mon YYYY HH:MM:SS.mmm)

    Returns:
        list: list containing:

            int array: sec, time array to plot the event occurences

            bool array: events, boolean array true = event is here, false = no event

            float: tot_time_event, max time in the GMAT file

            bool: true if no event occurs 

    sec and events are constructed so that you can plot the event occurences easily (Example : plot(sec,events))
    '''
    #Read data from file
    f = open(dataFilePath, "r")
    lines = f.readlines()
    f.close()
    #init variables
    init_seconds = 0
    init_date = datetime.strptime(initial_epoch, '%d %b %Y %H:%M:%S.%f')
    no_event = False
    tot_time_event = 0
    sec = [init_seconds]
    events = [False]
    #detect first date line
    found_first_line = False
    cpt = 0
    while not found_first_line and cpt < len(lines):
        if "Start Time" in lines[cpt]:
            found_first_line = True
        else:
            cpt = cpt + 1
    #compute events array to be plotted
    if not found_first_line:
        no_event = True
        tot_time_event = 0
    else:
        for l in lines[cpt+1:] :
            line_split = l.split("    ")
            if len(line_split) > 3:
                date_0 = datetime.strptime(line_split[0], '%d %b %Y %H:%M:%S.%f')
                date_1 = datetime.strptime(line_split[1], '%d %b %Y %H:%M:%S.%f')
                ds0 = (date_0-init_date).total_seconds()
                ds1 = (date_1-init_date).total_seconds()
                sec.append(ds0)
                sec.append(ds0)
                sec.append(ds1)
                sec.append(ds1)
                events.append(False)
                events.append(True)
                events.append(True)
                events.append(False)
                tot_time_event = ds1        
    sec = np.array(sec)
    events = np.array(events)
    return [sec, tot_time_event, events, no_event]

#-----------------------------------------------------------------------------

def computeVisibilityTime(contactData):
    contact = contactData[2]
    sec = contactData[0]
    tot_time = 0
    for u in range(len(contact)-2):
        if contact[u] == 1 and contact[u+1] == 1:
            tot_time = tot_time + sec[u+1] - sec[u]
    return tot_time

#-----------------------------------------------------------------------------

def computeGeneralTimeVector(xlim):
    '''Compute the total simulation time vector with step 1s. 
    
    Args:
        xlim (int): Total simulation time [sec]
    
    Returns:
        int array: seconds, Total simulation time array with step 1s

    '''
    seconds = np.arange(0, int(xlim)-1, 1)
    return seconds

#-----------------------------------------------------------------------------

def detectNoEvents(eclipse_data, contact_data):
    '''Detect if there are no eclipses and/or no contact during simulation.
    Break the simulation if no events occurs.

    Args: 
        eclipse_data (list): List returned by fdetectEvent for the EclipseLocator file
        contact_data (list): List returned by fdetectEvent for the ContactLocator file
        
    Returns:
        bool: False if events are detected
    '''

    no_contact = contact_data[3]
    no_eclipse = eclipse_data[3]
    res = False
    #Detect if there is no contact and/or no eclipse times during the simulation period
    if no_contact and no_eclipse:
        res=True
    else: 
        if no_eclipse:
            print("No eclipse time during this period")
        elif no_contact:
            print("No contact time during this period")
    return res

#-----------------------------------------------------------------------------

def checkCriteria(config, mode, xlim, om, priority, seconds_eclipse, seconds_contact, eclipse, contact, inEclipse):
    """Compute active OMs based on the input criteria.

    Hypothesis: 
       -  circular orbit (TO BE EXPANDED)

    Args: 
        mode list
        xlim (int): Max GMAT simulation time [sec]
        om: dict containing om properties
        eclipse_data (list): List returned by fdetectEvent for the EclipseLocator file
        contact_data (list): List returned by fdetectEvent for the ContactLocator file
        inEclipse (list): List with flags for defining when in eclipse 

    Returns:
        mode
        inEclipse
        priority
    """
    
    if om['isEclipse']:
        for i in range(len(seconds_eclipse)-2):
            if eclipse[i] == True and eclipse[i+1] == True:
                inEclipse[int(seconds_eclipse[i]) : int(seconds_eclipse[i+1])] = 1
                if om['priority'] < priority[int(seconds_eclipse[i])]:
                    mode[int(seconds_eclipse[i]) : int(seconds_eclipse[i+1])] = int(om['modeID'])
                    priority[int(seconds_eclipse[i]) : int(seconds_eclipse[i+1])] = int(om['priority'])    
    elif om['isInInterval']:
        mode, priority = getTimeInterval(config, mode, om, priority, xlim)
    elif om['isInRange']:
        mode, priority = getAnomalyRange(config, mode, om, priority, xlim)
    elif om['isOnTarget']:
        for u in range(len(seconds_contact)-2):
            if contact[u] == True and contact[u+1] == True:
                if om['priority'] < priority[int(seconds_contact[u])]:
                    mode[int(seconds_contact[u]) : int(seconds_contact[u+1])] = int(om['modeID'])
                    priority[int(seconds_contact[u]) : int(seconds_contact[u+1])] = int(om['priority'])    

    return mode, inEclipse, priority

#-----------------------------------------------------------------------------

def getAnomalyRange(config, mode, om, priority, xlim):
    """Compute active OMs while on specific true anomaly range over the simulation period.

    Hypothesis: 
       -  circular orbit (TO BE EXPANDED)

    Args: 
        mode list
        xlim (int): Max GMAT simulation time [sec]

    Returns:
        mode list
    """

    rangeStart = om['Range_start']*np.pi/180
    rangeEnd = om['Range_end']*np.pi/180

    period = 2*np.pi*np.sqrt((config["ORBIT"]["SMA"]**3)/398600)
    angVel = 2*np.pi/period
    an = config["ORBIT"]["TA"]*np.pi/180
    for i in range(int(xlim)):
        for n in range(len(rangeStart)):
            if an >= rangeStart[n] and an <= rangeEnd[n]:
                if om['priority'] < priority[i]:
                    mode[i] = int(om['modeID'])
                    priority[i] = int(om['priority'])
        an += angVel    
        if an >= 2*np.pi:
            an -= 2*np.pi

    return mode, priority

#-----------------------------------------------------------------------------

def getTimeInterval(config, mode, om, priority, xlim):
    """Compute active OMs while in specific time periods over the simulation period.

    Hypothesis: 
       -  circular orbit (TO BE EXPANDED)

    Args: 
        mode list
        xlim (int): Max GMAT simulation time [sec]

    Returns:
        mode list
    """

    intervalStart = om['intervalStart']
    intervalEnd = om['intervalEnd']    

    period = 2*np.pi*np.sqrt((config["ORBIT"]["SMA"]**3)/398600)
    angVel = 2*np.pi/period
    an = config["ORBIT"]["TA"]*np.pi/180  # in radians
    actTime = an/angVel
    for i in range(int(xlim)):
        if actTime >= intervalStart and actTime <= intervalEnd:
            if om['priority'] < priority[i]:
                mode[i] = int(om['modeID'])
                priority[i] = int(om['priority']) 
        actTime += 1
        an += angVel    # timestep = 1s
        if actTime >= period:
            actTime -= period

    return mode, priority

#-----------------------------------------------------------------------------

def computeBatteryConsumption(xlim, eclipse_data, contact_data, TotBatPower, SolarPower, config):

    """Compute Battery DOD over the simulation period.

    Hypothesis: 
       -  All eclipses are considered as dark (umbra)

    Args: 
        xlim (int): Max GMAT simulation time [sec]
        eclipse_data (list): List returned by fdetectEvent for the EclipseLocator file
        contact_data (list): List returned by fdetectEvent for the ContactLocator file
        TotBatPower (float): Total battery energy [Wh]
        SolarPower (float): Power given by the solar panels [W]
        Config (orderedDict): configuration for operative modes definition

    Returns:
        list: list containing:

            int array: mode, describe satellite mode at each second (0: Charge 1 : Mesure 2 : Vidage station)

            float array: remaining_power, Remaining power in the battery at each second during simulation [Wh]
    """

    seconds_eclipse = eclipse_data[0]
    seconds_contact = contact_data[0]

    eclipse = eclipse_data[2]
    contact = contact_data[2]


    for i in range(int(config['OPERATING_MODES']['N_of_modes'])):
        if config['OPERATING_MODES']['MODE_'+str(i)]['isDefault']:
            mode = np.ones(int(xlim))*i
            
    inEclipse = np.zeros(int(xlim))
    priority = np.ones(int(xlim))*(int(config['OPERATING_MODES']['N_of_modes']+ 1))

    for i in range(int(config['OPERATING_MODES']['N_of_modes'])):
        mode, inEclipse, priority = checkCriteria(config, mode, xlim, config['OPERATING_MODES']['MODE_'+str(i)], priority, seconds_eclipse, seconds_contact, eclipse, contact, inEclipse)

    # Compute remaining power
    remaining_power = [TotBatPower]
    last_Val = TotBatPower

    # cons_coeff = [config['OPERATING_MODES']["MODE_0"]["powerCons"]/3600]
    # charge_coeff = [(SolarPower - config['OPERATING_MODES']["MODE_0"]["powerCons"])/3600]

    cons_coeff = []
    charge_coeff = []

    for modeIter in range(config["OPERATING_MODES"]["N_of_modes"]):
        cons_coeff.append(config["OPERATING_MODES"]["MODE_"+str(modeIter)]["powerCons"]/3600)
        charge_coeff.append((SolarPower - config["OPERATING_MODES"]["MODE_"+str(modeIter)]["powerCons"])/3600)

    for s in range(int(xlim)): 
        if inEclipse[s] == 1:
            last_Val = last_Val - cons_coeff[int(mode[s])]
        else:
            if last_Val < TotBatPower:
                    last_Val = last_Val + charge_coeff[int(mode[s])]
            else:
                last_Val = TotBatPower
        
        remaining_power.append(last_Val)
    
    return [mode, remaining_power]

#-----------------------------------------------------------------------------

def loopVerbose(i,scale=1000,graph="default"):
    if i % scale == 0:
        print (graph+" step: "+str(i)) 

#-----------------------------------------------------------------------------

def plotBatteryConsumption(config,eclipse_data, contact_data, seconds, xlim, mode, remaining_power,TotBatPower):
    """Plot battery DOD, eclipses and contact times

    Args:
        eclipse_data (list): List returned by fdetectEvent for the EclipseLocator file
        contact_data (list): List returned by fdetectEvent for the ContactLocator file
        seconds (int array): Time vector with 1s steps from 0 to total simulation time
        xlim (int): Max GMAT simulation time [sec]
        mode (int array): Describe satellite mode at each second (0: Charge 1 : Mesure 2 : Vidage station)
        remaining_power (float array): Remaining power in the battery at each second [Wh]
        TotBatPower (float): Total battery energy [Wh]

    Returns: 
        str: fig_path, path to the resulting saved figure  
    """
    print("simulation length: "+str(len(seconds)) + "[seconds]")
    visibility_time = computeVisibilityTime(contact_data)
    
    seconds_eclipse = eclipse_data[0]
    eclipse = eclipse_data[2]

    seconds_contact = contact_data[0]
    contact = contact_data[2]
    
    fig, axs = plt.subplots(3, 1)
    #fig.suptitle('Battery data')
    #fig.tight_layout() # Or equivalently,  "plt.tight_layout()"
    
    #Eclipses
    print("Drawing eclispes")
    axs[0].plot(seconds_eclipse, eclipse)
    axs[0].set_xlabel('time [s]')
    axs[0].set_ylabel('eclipse')

    

    axs[0].set_xlim(0,xlim)
    axs[0].grid(True)

    #Visibility
    print("Drawing Visibility")

    grayColor = '#D6EFFF' 
    redColor = '#BA525270'
    greenColor = '#00990040'    
    
    wasInEclipse = False
    borneInfList = []
    borneSupList = []
    
    for i in range(len(seconds_eclipse)-2):
        #loopVerbose(i,graph="eclipse")
               
        if eclipse[i] == True :
            borneInf = seconds_eclipse[i]          
            if not wasInEclipse:
                wasInEclipse = True
                borneInfList.append(borneInf)  
               
            borneSup = seconds_eclipse[i+1]
            if eclipse[i+1] == False:
                
                borneSupList.append(borneSup)
                wasInEclipse = False        
            
    if wasInEclipse:
        borneSupList.append(borneSup)
    
    
    print("eclipse length: "+str(len(borneInfList)))
    if not onlyTest:
        for i in range(len(borneInfList)):
            #axs[1].axvspan(borneInf, borneSup, facecolor=grayColor,edgecolor= grayColor)
            loopVerbose(i,graph="eclipse")
            axs[1].axvspan(borneInfList[i], borneSupList[i], facecolor=grayColor,edgecolor= grayColor)
    

    #plt.show()
        
       
    axs[1].plot(seconds_contact, contact, 'r')
    axs[1].set_xlabel('time [s]')
    axs[1].set_ylabel('\n visibility - TLS')
    axs[1].set_xlim(0,xlim)
    axs[1].grid(True)

    strTC = 'Total Visibility time: \n' + "{:.2f}".format(visibility_time/60) + 'min'
    axs[1].text(0.1,0.5, strTC)#, bbox=dict(facecolor='red', alpha=0.5))

    gray_patch = mpatches.Patch(color=grayColor, label='Eclipse')
    red_patch = mpatches.Patch(color=redColor, label='Sending TM')
    axs[1].legend(handles=[gray_patch,red_patch],loc=7)

    #Satellite modes
    print("Drawing Modes")


    borneInf = 0
    borneSup = 0
    
    
    # if not onlyTest:
    #     for i in range(len(seconds)-2):
    #         loopVerbose(i,graph="modes")
    #         if mode[borneInf] == mode[borneSup]:
    #             if i == len(seconds)-3:
    #                 if mode[borneInf] == 0 :
    #                     axs[2].axvspan(borneInf, borneSup, facecolor=greenColor,edgecolor= greenColor)
    #                 elif mode[borneInf] == 1:
    #                     axs[2].axvspan(borneInf, borneSup, facecolor= grayColor,edgecolor= grayColor)
    #                 else:
    #                     axs[2].axvspan(borneInf, borneSup, facecolor= redColor,edgecolor= redColor)
    #             borneSup = borneSup + 1
    #         else:
    #             if mode[borneInf] == 0 :
    #                 axs[2].axvspan(borneInf, borneSup, facecolor=greenColor,edgecolor= greenColor)
    #             elif mode[borneInf] == 1:
    #                 axs[2].axvspan(borneInf, borneSup, facecolor= grayColor,edgecolor= grayColor)
    #             else:
    #                 axs[2].axvspan(borneInf, borneSup, facecolor= redColor,edgecolor= redColor)
    #             borneInf = borneSup

    if not onlyTest:
        for i in range(len(seconds)-2):     # why -2 ?
            loopVerbose(i,graph="modes")
            if mode[borneInf] == mode[borneSup]:
                if i == len(seconds)-3:
                    # if mode[borneInf]==0:
                    #     axs[2].axvspan(borneInf, borneSup, \
                    #                facecolor= config["MODE_DEFAULT"]["color"], \
                    #                 edgecolor= config["MODE_DEFAULT"]["color"])
                    # else:
                    axs[2].axvspan(borneInf, borneSup, \
                                    facecolor= config["OPERATING_MODES"]["MODE_"+str(int(mode[borneInf]))]["postProColor"], \
                                        edgecolor= config["OPERATING_MODES"]["MODE_"+str(int(mode[borneInf]))]["postProColor"])
                borneSup = borneSup + 1
            else:
                # if mode[borneInf]==0:
                #     axs[2].axvspan(borneInf, borneSup, \
                #                    facecolor= config["MODE_DEFAULT"]["color"], \
                #                     edgecolor= config["MODE_DEFAULT"]["color"])
                # else:
                axs[2].axvspan(borneInf, borneSup, \
                               facecolor= config["OPERATING_MODES"]["MODE_"+str(int(mode[borneInf]))]["postProColor"], \
                                edgecolor= config["OPERATING_MODES"]["MODE_"+str(int(mode[borneInf]))]["postProColor"])
                borneInf = borneSup    

        # patches = [mpatches.Patch(color = config["MODE_DEFAULT"]["color"],\
        #                            label = config["MODE_DEFAULT"]["ModeName"]) ]

        patches = []

        for modeIter in range(config["OPERATING_MODES"]["N_of_modes"]):
            patches.append( mpatches.Patch(color = config["OPERATING_MODES"]["MODE_"+str(int(modeIter))]["postProColor"],\
                                   label = config["OPERATING_MODES"]["MODE_"+str(int(modeIter))]["modeName"]) )

        axs[2].plot(seconds, np.array(remaining_power[1:-1])*(100/TotBatPower))
        axs[2].legend(handles=patches,loc='right')
        
        axs[2].set_xlabel('time [s]')
        axs[2].set_ylabel('Battery Capacity [%]') # DOD is 100 - left capacity [%]
        axs[2].set_xlim(0,xlim)
        axs[2].grid(True)
        
    
    myDateTime = datetime.today().strftime('%Y-%m-%d_%H-%M-%S')
    fig_path = config['paths']['output_dir']+'Figures/BatteryRemainingPower_' + myDateTime+ '.pdf'
    plt.savefig(fig_path)
    
    fig_path = config['paths']['output_dir']+'Figures/BatteryRemainingPower_' + myDateTime + '.png'
    plt.savefig(fig_path)  
    
    plt.show()
    
    return fig_path

#-----------------------------------------------------------------------------

def getGMATepochformat(epoch):
    '''Return the given epoch with the format used in GMAT scripts. 

    Args:
       epoch (str): Time epoch, format : YYYY,MM,DD,hh,mm,ss
    
    Returns:
       str: initial epoch, format : DD Mon YYYY HH:MM:SS.mmm

    '''

    months = ['Jan','Feb','Mar','Apr','May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov','Dec']
    initial_epoch = (epoch[8:10] + " " + months[int(epoch[5:7]) - 1] + " " + epoch[0:4] + " " 
                    + epoch[11:13] + ":" + epoch[14:16] + ":" +epoch[17:19] + ".000")
    
    return initial_epoch

#-----------------------------------------------------------------------------

def writeGMATscript(config):
    '''Write a new GMAT script with orbit and simulation parameters given. 
    The default GMAT script used is DefaultSat.script file. If you want to change 
    any other parameter than the orbit or the simulation time, open DefaultSat.script in GMAT,
    change the data, and save the script before running this simulation.

    The new GMAT script is saved as MissionFromPython.script in config's output_dir

    Args: 
        config (dict): config file containing all keplerian parameters, epoch and path, Cf. input.yaml file

    '''
    epoch = config["ORBIT"]["epoch"]
    SMA = str(config["ORBIT"]["SMA"])
    ECC = str(config["ORBIT"]["ECC"])
    INC = str(config["ORBIT"]["INC"])
    RAAN = str(config["ORBIT"]["RAAN"])
    AOP = str(config["ORBIT"]["AOP"])
    TA = str(config["ORBIT"]["TA"])
    tot_time = config['SIMULATION']['total_time']
    GS_Data = {"GS_Altitude": config['GROUNDSTATION_LOCATION']['GS_altitude'],
               "GS_Elevation": config['GROUNDSTATION_LOCATION']['GS_minElevation'],
               "GS_Latitude": config['GROUNDSTATION_LOCATION']['GS_latitude'],
               "GS_Longitude": config['GROUNDSTATION_LOCATION']['GS_longitude']
    }
    dt = config['SIMULATION']['dt']
    
    #Get default CREME data from GMAT script
    fileNameInit = config["paths"]["input_dir"]+'DefaultSat.script'
    file_open = open(fileNameInit,'r')
    lines = file_open.readlines()
    file_open.close()

    #replace default orbit data in the GMAT script
    fileNameGmat = config["paths"]["output_dir"]+'MissionFromPython.script'
    
    file_open2 = open(fileNameGmat,'w')

    initial_epoch = getGMATepochformat(epoch)

    for u in range(len(lines)):
        if 'GMAT DefaultSat.SMA =' in lines[u] : 
            lines[u] = 'GMAT DefaultSat.SMA = ' + SMA + ";\n"
        elif 'GMAT DefaultSat.ECC =' in lines[u] : 
            lines[u] = 'GMAT DefaultSat.ECC = ' + ECC + ";\n"
        elif 'GMAT DefaultSat.INC =' in lines[u] : 
            lines[u] = 'GMAT DefaultSat.INC = ' + INC + ";\n"
        elif 'GMAT DefaultSat.RAAN =' in lines[u] : 
            lines[u] = 'GMAT DefaultSat.RAAN = ' + RAAN + ";\n"
        elif 'GMAT DefaultSat.AOP =' in lines[u] : 
            lines[u] = 'GMAT DefaultSat.AOP = ' + AOP + ";\n"
        elif 'GMAT DefaultSat.TA =' in lines[u] : 
            lines[u] = 'GMAT DefaultSat.TA = ' + TA + ";\n"
        elif 'GMAT DefaultSat.Epoch =' in lines[u] : 
            lines[u] = "GMAT DefaultSat.Epoch = '" + initial_epoch +"';\n"
        elif 'GMAT DefaultGS.Location1 =' in lines[u] : 
            lines[u] = "GMAT DefaultGS.Location1 = " + str(GS_Data["GS_Latitude"]) +";\n"
        elif 'GMAT DefaultGS.Location2 =' in lines[u] : 
            lines[u] = "GMAT DefaultGS.Location2 = " + str(GS_Data["GS_Longitude"]) +";\n"
        elif 'GMAT DefaultGS.Location3 =' in lines[u] : 
            lines[u] = "GMAT DefaultGS.Location3 = " + str(GS_Data["GS_Altitude"]*0.001) +";\n"
        elif 'GMAT DefaultGS.MinimumElevationAngle =' in lines[u] : 
            lines[u] = "GMAT DefaultGS.MinimumElevationAngle = " + str(GS_Data["GS_Elevation"]) +";\n"
        elif 'Propagate Propagator1(DefaultSat)' in lines[u]:
            lines[u] = 'Propagate Propagator1(DefaultSat) {DefaultSat.ElapsedDays = ' + str(tot_time) + '};\n'
        elif 'GMAT Propagator1.InitialStepSize' in lines[u]:
            lines[u] = 'GMAT Propagator1.InitialStepSize = ' + str(dt) + ';\n'
        elif 'GMAT Propagator1.MinStep' in lines[u]:
            lines[u] = 'GMAT Propagator1.MinStep = ' + str(dt) + ';\n'     
        elif 'GMAT Propagator1.MaxStep' in lines[u]:
            lines[u] = 'GMAT Propagator1.MaxStep = ' + str(dt) + ';\n'      
        file_open2.write(lines[u])

    #Close all opened files
    file_open2.close()

#-----------------------------------------------------------------------------

def runMissionFromPython(GMAT_BIN, bGUI):
    '''Launch MissionFromPython.script (file automatically constructed by the application and saved in GMAT/DefaultCubesat) 
    in GMAT and run it.

    Args: 
        GMAT_BIN (str): path to the GMAT application executable file
    '''

    if bGUI:
        os_res = os.system(GMAT_BIN + ' --run ../GMAT/Cubesat_DO_NOT_MODIFY/MissionFromPython.script')
    else:
        os_res = os.system(GMAT_BIN + ' --minimize --run ../GMAT/Cubesat_DO_NOT_MODIFY/MissionFromPython.script --exit')
    
    if os_res != 0:
        exit('ERROR : Verify the path to your GMAT application in the input file')

    #elif platform.system() == 'Linux':
        #os.system('./../GMAT_Application/bin/GMAT-R2020a --run ../GMAT/DefaultCubesat/MissionFromPython.script --exit')
    #    os_res = os.system(GMAT_BIN + ' --minimize --run ../GMAT/DefaultCubesat/MissionFromPython.script --exit')
    #    if os_res != 0:
    #        exit('ERROR : Verify the path to your GMAT application in the input file')     

#-----------------------------------------------------------------------------

def runMissionFromPythonInDocker(config,runGMAT = False):
    '''Launch MissionFromPython.script (file automatically constructed by the application and saved in GMAT/DefaultCubesat) 
    in GMAT running into a docker and run it.

    Args: 
        GMAT_docker (str): docker command to execute the script
    '''  
    #os_res = os.system(GMAT_docker)
    #temporary horrible hack
    #calue hard coded in GMAT script... shoumd be changed !
    #GMAT_docker = config['paths']['GMAT_BIN_linux_docker']
  
    if runGMAT:
        print("GMAT running... check contactLocator output !")
        manuallineToExec = "docker run -it  --mount type=bind,source=/home/glucc/tesi/intern-2023-giacomo-luccisano-preliminarydesign/python_scripts/Output_Lite,target=/app/Output_Lite --rm  gmat:gui \"/app/R2020a/bin/GmatConsole --run /app/Output_Lite/MissionFromPython.script\""
        print(manuallineToExec)

        lineToExec = "docker run  --mount type=bind,source=/home/glucc/tesi/intern-2023-giacomo-luccisano-preliminarydesign/python_scripts/Output_Lite,target=/app/Output_Lite --rm  gmat:gui \"/app/R2020a/bin/GmatConsole --run /app/Output_Lite/MissionFromPython.script\""
    
    else:
        print("GMAT NOT running...  copying files")
        lineToExec = "cp Data/intermediaryResults/ContactLocator1.txt "+config['paths']['output_dir']+"ContactLocator1.txt"
        
        
    os_res = os.system(lineToExec)
    if os_res != 0:
        exit('ERROR : Verify the path to your GMAT outputs intermediary results')      

    if not runGMAT:
        lineToExecCP = "cp Data/intermediaryResults/EclipseLocator1.txt "+config['paths']['output_dir']+"EclipseLocator1.txt"
        os_res = os.system(lineToExecCP)
    
    
    if os_res != 0:
        exit('ERROR : Verify the path to your GMAT outputs intermediary results')    
    
#-----------------------------------------------------------------------------    

def computeDataQuantities(contact_data, TC_debit_nominal, TM_debit_nominal, mode, config):
    '''Compute TeleCommand and TeleMetry quantities that can be uploaded/downloaded
    during visibilties given nominal debits.

    Args: 
        contact_data (list): List returned by fdetectEvent for the ContactLocator file
        TC_debit_nominal (float): LIST Nominal TC rate between satellite and antenna [kbits/s]
        TM_debit_nominal (float): LIST Nominal TM rate between satellite and antenna [kbits/s]
        mode (list): List returned by fcomputeBatteryConsumption with the mode active for each time step


    Returns:
        list: list containing:

        int array: contactTC, Array describing the amount of TC that can be transferred during visibility [Mbits]

        int array: contactTM, Array describing the amount of TM that can be transferred during visibility [Mbits]
    

    '''

    seconds_contact = contact_data[0]
    contact = contact_data[2]

    contactTC = 1.*contact
    contactTM = 1.*contact
    for u in range(len(contact)-1):
        if contact[u] == True and contact[u+1] == True:
            ds = seconds_contact[u+1] - seconds_contact[u]
            qTC = TC_debit_nominal[int(mode[int(seconds_contact[u])])]*ds*0.001 
            qTM = TM_debit_nominal[int(mode[int(seconds_contact[u])])]*ds*0.001
            contactTC[u] = qTC
            contactTC[u+1] = qTC
            contactTM[u] = qTM
            contactTM[u+1] = qTM
    return [contactTC, contactTM]

#-----------------------------------------------------------------------------

def plotDataBudget(config,contact_data, contactTC, contactTM):
    '''Plot TC and TM data budgets.

    Args: 
        contact_data (list): List returned by fdetectEvent for the ContactLocator file
        contactTC (int array): Array describing the amount of TC that can be transferred during visibility [Mbits] (returned by fcomputeDataQuantities)
        contactTM (int array): Array describing the amount of TM that can be transferred during visibility [Mbits] (returned by fcomputeDataQuantities)

    Returns: 
        str: fig_path, path to the resulting saved figure 
    '''
    seconds_contact = contact_data[0]

    xlim = seconds_contact[-1] + 1000

    fig, axs = plt.subplots(2, 1)
    axs[0].plot(seconds_contact, contactTC)
    axs[0].set_xlabel('time [s]')
    axs[0].set_ylabel('TC quantities - TLS  [Mb]', fontsize = 8)
    axs[0].set_xlim(0,xlim) 
    axs[0].grid(True)
    strTC = 'Total TC during period : ' + "{:.2f}".format(np.sum(contactTC)/2.) + 'Mb'
    axs[0].text(0.5,np.max(contactTC)-2, strTC, bbox=dict(facecolor='red', alpha=0.5))
        
    
    axs[1].plot(seconds_contact, contactTM)
    axs[1].set_xlabel('time [s]')
    axs[1].set_ylabel('TM quantities - TLS  [Mb]', fontsize = 8)
    axs[1].set_xlim(0,xlim)
    axs[1].grid(True)
    strTM = 'Total TM during period : ' + "{:.2f}".format(np.sum(contactTM)/2.) + 'Mb'
    axs[1].text(0.5,np.max(contactTM)-2, strTM, bbox=dict(facecolor='red', alpha=0.5))
    
    
    myDateTime = datetime.today().strftime('%Y-%m-%d_%H-%M-%S')
    fig_path = config['paths']['output_dir']+'Figures/DataBudget_' + myDateTime+ '.pdf'
    plt.savefig(fig_path)
    
    
    fig_path = config['paths']['output_dir']+'Figures/DataBudget_' + myDateTime+ '.png'
    plt.savefig(fig_path)    
    
    plt.show()

    return fig_path

#-----------------------------------------------------------------------------

def saveReport(config,config_for_report, battery_fig_path, data_fig_path, stringSyntheseLinkBudget, visibility_data, max_eclipse_time, power_output_for_report):

    '''Save Markdown report

    Args:
        datadict (dictionnary of tuples): all input mission data and units
        battery_fig_path (str): Path to the battery consumption result figure
        data_fig_path (str): Path to the data budget result figure
    '''

    fname = config['paths']['output_dir'] + config_for_report['SIMULATION']['report_name'][0] +'_'  + datetime.today().strftime('%Y-%m-%d_%H-%M-%S')
    mdFile = MdUtils(file_name=fname,title='Mission Analysis Report')
    del config_for_report['paths']
    
    #Inputs listing
    mdFile.new_header(level=1, title='Mission Inputs')
    list_of_strings = ["Category", "Data", "Value", "Unit"]
    for key,value in config_for_report.items():# - {'paths'}:
        list_of_strings.extend([key, '', '', ''])
        for key_1 in config_for_report[key]:
            list_of_strings.extend(['', key_1, str(config_for_report[key][key_1][0]), str(config_for_report[key][key_1][1])])
    mdFile.new_table(columns=4, rows=int(len(list_of_strings)/4), text=list_of_strings, text_align='center')
    
    #Power budget
    mdFile.new_header(level =1, title='Power analysis')
    mdFile.new_line("Max eclipse time during period = {0:.2f} s / {1:.0f} min".format(max_eclipse_time, max_eclipse_time/60))
    image = mdFile.new_inline_image("PowerAnalysis", battery_fig_path)
    mdFile.new_line(image)

    #Data budget
    mdFile.new_header(level =1, title='Data analysis')
    mdFile.new_line("Max visibility during period = {0:.2f} s / {1:.0f} min".format(visibility_data[0], visibility_data[0]/60))
    mdFile.new_line("Min visibility during period = {0:.2f} s / {1:.0f} min".format(visibility_data[1], visibility_data[1]/60))
    mdFile.new_line("Mean visibility during period = {0:.2f} s / {1:.0f} min".format(visibility_data[2], visibility_data[2]/60))
    mdFile.new_line("Max visibility during a day and during period = {0:.2f} s / {1:.0f} min".format(visibility_data[3], visibility_data[3]/60))
    mdFile.new_line("Min visibility during a day and during period = {0:.2f} s / {1:.0f} min".format(visibility_data[4], visibility_data[4]/60))
    mdFile.new_line("Mean visibility per day during period = {0:.2f} s / {1:.0f} min".format(visibility_data[5], visibility_data[5]/60))
    image = mdFile.new_inline_image("DataAnalysis", data_fig_path)
    mdFile.new_line(image)

    #Link budget
    mdFile.new_header(level =1, title='Link analysis')
    mdFile.new_line(stringSyntheseLinkBudget)
    list_of_strings = ["Category", "Data", "Value", "Unit"]
    for key,value in power_output_for_report.items():# - {'paths'}:
        list_of_strings.extend([key, '', '', ''])
        for key_1 in power_output_for_report[key]:
            list_of_strings.extend(['', key_1, str(power_output_for_report[key][key_1][0]), str(power_output_for_report[key][key_1][1])])
    mdFile.new_table(columns=4, rows=int(len(list_of_strings)/4), text=list_of_strings, text_align='center')

    #Create md report
    mdFile.create_md_file()
    

    lineToExec = "cp "+fname+ ".md "+config['paths']['output_dir']+"lastReport.md"
    os_res = os.system(lineToExec)
    if os_res != 0:
        exit('ERROR : Verify the path of your output report file')    
    else:
        print("you can now run: \ngrip lastReport.md")

#-----------------------------------------------------------------------------

def printException(e) : 
    '''Print exception details.

    Args: 
        e (class Exception):
    '''
    exc_type, exc_obj, exc_tb = sys.exc_info()
    print('-------')
    print(traceback.format_exc())

#-----------------------------------------------------------------------------

def delUnits(config_dict):

    '''Convert yaml strings inputs with units to float in a config dictionnary

    Args:
        config_dict (dict): input dictionnary loaded from the yaml input file
    
    Returns:
        dict: new_config_without_unit, same dictionnary but without the unit in the values
    '''

    new_config_without_unit = copy.deepcopy(config_dict)
    for key,value in config_dict.items():
        for key_1,value_1 in value.items():
            if type(value_1) is str:
                if '[' in value_1 :
                    new_value = value_1[:-1].split('[')
                    new_config_without_unit[key][key_1] = float(new_value[0])
                else:
                    new_config_without_unit[key][key_1] = value_1
            else:
                new_config_without_unit[key][key_1] = value_1
    return new_config_without_unit

#-----------------------------------------------------------------------------

def sepUnits(config_dict):
    '''Separate units from value in the input dictionnary.

    Args:
        config_dict (dict): input dictionnary loaded from the yaml input file
    
    Returns:
        dict: new_config_split_unit, same dictionnary but with tuples values (value, 'unit')
    '''

    new_config_split_unit = copy.deepcopy(config_dict)
    for key,value in config_dict.items():
        for key_1,value_1 in value.items():
            if type(value_1) is str:
                if '[' in value_1 :
                    new_value = value_1[:-1].split('[')
                    new_config_split_unit[key][key_1] = new_value
                else:
                    new_value = value_1
                    new_config_split_unit[key][key_1] = [new_value, '']
            else:
                new_value = value_1
                new_config_split_unit[key][key_1] = [new_value, '']
    return new_config_split_unit

#-----------------------------------------------------------------------------

def findVisibility(datafilepath, no_event, nb_days):
    if no_event:
        print('ffindVisibility : Nothing to compute as no contact has been found.')
    else:
        #Read data from file
        f = open(datafilepath, "r")
        lines = f.readlines()
        f.close()
        data_array = []
        #detect first date line
        found_first_line = False
        cpt = 0
        while not found_first_line and cpt < len(lines):
            if "Start Time" in lines[cpt]:
                found_first_line = True
            else:
                cpt = cpt + 1
        first_loop = True  
        contact_sum_per_day = 0
        contact_per_day_array = []
        for l in lines[cpt+1:] :
            line_split = l.split("    ")
            if len(line_split) > 3:
                day = int(line_split[0].split(" ")[0])
                contact_sec = float(line_split[2])
                if first_loop: 
                    day_ini = day
                if day == day_ini:
                    contact_sum_per_day += contact_sec
                else: 
                    contact_per_day_array.append(contact_sum_per_day)
                    contact_sum_per_day = 0
                    contact_sum_per_day += contact_sec
                    day_ini = day
                first_loop = False
                data_array.append(contact_sec)
        contact_per_day_array.append(contact_sum_per_day)

        max_visibility = np.max(data_array)
        min_visibility = np.min(data_array)
        mean_visibility = np.mean(data_array)
        max_visibility_per_day = np.max(contact_per_day_array)
        min_visibility_per_day = np.min(contact_per_day_array)
        mean_visibility_per_day = np.mean(contact_per_day_array)

        #plt.plot(np.arange())
        #plt.plot()
        

    return [max_visibility, min_visibility, mean_visibility, max_visibility_per_day, min_visibility_per_day, mean_visibility_per_day]

#-----------------------------------------------------------------------------

def findMaxEclipse(eclipsefilepath, no_event):
    if no_event:
        print('ffindMaxEclipse : Nothing to compute as no eclipse has been found -- returning 0.')
        res = 0
    else:
        #Read data from file
        f = open(eclipsefilepath, "r")
        lines = f.readlines()
        f.close()
        #detect first date line
        found_max_duration_line = False
        cpt = 0
        while not found_max_duration_line and cpt < len(lines):
            if "Maximum duration" in lines[cpt]:
                found_max_duration_line = True
                line_max_duration = lines[cpt]
            else:
                cpt = cpt + 1
        line_max_duration = line_max_duration.split(":")
        res = float(line_max_duration[1])
    return res

#-----------------------------------------------------------------------------

def compute_path_loss_from_distance(d, f, c, Lp, La, Mp): 
    FSL = 10*np.log10((4*np.pi*d*1000*f/c) **2)
    path_loss = FSL + Lp + La + Mp
    return path_loss

#-----------------------------------------------------------------------------

def getAltitudeMax(config):
    R_e_km=6378.136
    altitude_max = (float(str(config["ORBIT"]["ECC"])) + 1)*float(str(config["ORBIT"]["SMA"])) -R_e_km
    return altitude_max

#-----------------------------------------------------------------------------

def computeLinkBudget(config):
    '''Compute full link budget figures for the given configuration (dictionnary from the yaml input file)

    Args: 
        config (dic): yaml load dictionary from input file (Data/input.yaml) 

    Returns:
        dict: report of the loink budget well formated    

    '''
    #Common
    Gs = config['SPACECRAFT_TRANSMITTER']['SC_ant_gain_tx']
    Ge = config['GROUND_STATION_TRANSMITTER']['GS_ant_gain_tx']

    min_elevation = config['GROUNDSTATION_LOCATION']['GS_minElevation']

    c = config['CONSTANTS']['c']
    Re = config['CONSTANTS']['radius_earth']

    Lp_worst = config['PROPAGATION_LOSSES']['loss_pol']
    Lp_best = 0
    La_worst = config['PROPAGATION_LOSSES']['loss_atm']
    La_best = 0
    Mp_worst = config['PROPAGATION_LOSSES']['loss_misc']
    Mp_best = 0

    #Uplink
    f_uplink = config['UPLINK']['frequency']
    Ptx_uplink = 10*np.log10(config['GROUND_STATION_TRANSMITTER']['GS_power_tx']/0.001)
    Lrx_uplink_worst = config['SPACECRAFT_RECEIVER']['SC_loss_cable_rx'] + config['SPACECRAFT_RECEIVER']['SC_loss_connector_rx'] +\
          config['SPACECRAFT_RECEIVER']['SC_loss_point_rx']
    Lrx_uplink_best = config['SPACECRAFT_RECEIVER']['SC_loss_cable_rx'] + config['SPACECRAFT_RECEIVER']['SC_loss_connector_rx']
    Ltx_uplink_worst = config['GROUND_STATION_TRANSMITTER']['GS_line_loss_tx'] + config['GROUND_STATION_TRANSMITTER']['GS_loss_connector_tx'] +\
         config['GROUND_STATION_TRANSMITTER']['GS_loss_point_rx']
    Ltx_uplink_best = config['GROUND_STATION_TRANSMITTER']['GS_line_loss_tx'] + config['GROUND_STATION_TRANSMITTER']['GS_loss_connector_tx']
    altitude_max = getAltitudeMax(config)
    path_loss_worst_uplink = compute_path_loss_from_distance(getSlantRange(altitude_max,min_elevation,Re), f_uplink, c, Lp_worst, La_worst, Mp_worst) 
    path_loss_best_uplink = compute_path_loss_from_distance(altitude_max, f_uplink, c, Lp_best, La_best, Mp_best)

    Power_rx_uplink_worst = Ptx_uplink + Gs + Ge - path_loss_worst_uplink - Lrx_uplink_worst - Ltx_uplink_worst
    Power_rx_uplink_best =  Ptx_uplink + Gs + Ge - path_loss_best_uplink - Lrx_uplink_best - Ltx_uplink_best

    #Downlink
    f_downlink = config['DOWNLINK']['frequency']
    Ptx_downlink = 10*np.log10(config['SPACECRAFT_TRANSMITTER']['SC_power_tx']/0.001)
    Lrx_downlink_worst = config['GROUND_STATION_RECEIVER']['GS_loss_cable_D_rx'] + config['GROUND_STATION_RECEIVER']['GS_loss_connector_rx'] +\
          config['GROUND_STATION_RECEIVER']['GS_loss_point_rx']
    Lrx_downlink_best = config['GROUND_STATION_RECEIVER']['GS_loss_cable_D_rx'] + config['GROUND_STATION_RECEIVER']['GS_loss_connector_rx']
    Ltx_downlink_worst = config['SPACECRAFT_TRANSMITTER']['SC_loss_cable_tx'] + config['SPACECRAFT_TRANSMITTER']['SC_loss_connector_tx'] +\
          config['SPACECRAFT_TRANSMITTER']['SC_loss_point_tx'] + config['SPACECRAFT_TRANSMITTER']['SC_loss_feeder_tx']
    Ltx_downlink_best = config['SPACECRAFT_TRANSMITTER']['SC_loss_cable_tx'] + config['SPACECRAFT_TRANSMITTER']['SC_loss_connector_tx'] +\
          config['SPACECRAFT_TRANSMITTER']['SC_loss_feeder_tx']
    
    path_loss_worst_downlink = compute_path_loss_from_distance(getSlantRange(altitude_max,min_elevation,Re), f_downlink, c, Lp_worst, La_worst, Mp_worst) 
    path_loss_best_downlink = compute_path_loss_from_distance(altitude_max, f_downlink, c, Lp_best, La_best, Mp_best)

    Power_rx_downlink_worst = Ptx_downlink + Gs + Ge - path_loss_worst_downlink - Lrx_downlink_worst - Ltx_downlink_worst
    Power_rx_downlink_best =  Ptx_downlink + Gs + Ge - path_loss_best_downlink - Lrx_downlink_best - Ltx_downlink_best

    LNA_gain = config['GROUND_STATION_RECEIVER']['GS_LNA_gain']
    Power_Satcore_input_worst = Power_rx_downlink_worst + LNA_gain
    Power_Satcore_input_best = Power_rx_downlink_best + LNA_gain

    power_output_for_report = {'UPLINK POWER':
    {
        'Minimal power at duplexer input' : [Power_rx_uplink_worst, 'dBm'],
        'Maximal power at duplexer input' : [Power_rx_uplink_best, 'dBm'],
    },
    'DOWNLINK_POWER':
    {
        'Minimal power at SATCORE input' : [Power_Satcore_input_worst, 'dBm'],
        'Maximal power at SATCORE input' : [Power_Satcore_input_best, 'dBm'],
    }
    }
    return power_output_for_report
