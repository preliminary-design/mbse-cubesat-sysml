# MBSE CubeSat SysML

_Author: Giacomo Luccisano. Please note that all the parser scripts are to be extensively tested and corrected. If you have any questions or want to signal a bug, you can [write me an e-mail](mailto:giacomo.luccisano@studenti.polito.it)_.

---

## Repository Structure

The files in this Repository are organized as follows:

- [gaphor_parser](./gaphor_parser/) has the python scripts for the Gaphor - Python parser
    - [tutorial](./gaphor_parser/tutorial/) contains a tutorial ([here](./gaphor_parser/tutorial/README.md)) on how to run the parser and how to generate a `.gaphor` model
- [python_scripts](./python_scripts/) collects the mission analysis scripts 

---

## Quickstart
### Only the Gaphor parser example
<img src="/gaphor_parser/tutorial/figs/system_stereotypes.png"> <img src="/gaphor_parser/tutorial/figs/relation_graph.png">

**AFTER installing Conda** ([here](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html) the download options) from the current folder you can run:

```
cd gaphor_parser    # move to gaphor_parser directory
                    # you should be in repo_installation_dir/gaphor_parser

python --version    # check your python version
                    # Python >= 3.10 is needed

conda create --name gaphor python=3.10.12
conda activate gaphor

conda install -c anaconda pyyaml 
conda install pip
pip install graphviz

python3 gaphorParser.py -h    # for some help...

# run the parser with heo_model example
# outputs will be generated in the outputs/ directory
python3 gaphorParser.py -i "examples/heo_model.gaphor" -o "outputs/"
``` 

If everything runs correctly you should have in your `modelOutDir` directory the following structure:
- `requirements_list.html` table with all requirements listed and alphabetically ordered;
- `table.css` style sheet for `requirements_list.html`, you can customise that as you wish;
- `relationGraph.gv` and `relationGraph.gv.pdf` **still WIP**, generated with [Graphviz](https://graphviz.org/) library, highlights the relationships between variables, to better understand what is affected by a change in the model;
- `components` folder:
    - `CommSys.yaml` with transmitter and receiver data;
    - `EPS.yaml` with solar array and battery data;
    - `GroundStation.yaml` with GS location and antenna data;
    - `OperatingMode.yaml` with the definition of the S/C operating modes;
    - `Orbit.yaml` with orbital parameters; 
    - `PropagationLosses.yaml` self explanatory I guess;
    - `SystemComp.yaml` with a list of all elements with `SystemComp` _stereotype_ applied. They are the components of the main systems (e.g., thrusters of the Propulsion System);
    - `SystemMain.yaml` same as `SystemComp.yaml`, but with the parents Systems properties (e.g., Propulsion System, Payload, ...);

### Mission Analysis tool
<img src ="./gaphor_parser/tutorial/figs/BatteryRemainingPower.png" style = "width = 50%"> <img src ="./gaphor_parser/tutorial/figs/DataBudget.png" style = "width = 50%">

### Dependencies
:one: For the mission analysis python scripts, everything can be found in [this folder](./python_scripts/) of the repository. It also includes the code main dependencies as NASA GMAT and the Gaphor parser itself.

:exclamation: Remember to change the paths references in the `CONFIG_input.yaml` file [here](./python_scripts/Data/) and in the `runMissionFromPythonInDocker` function inside the `functionsDatabase.py`. Everyhing should be straight forward.

:three:  Docker is needed for the correct execution of the `generalMissionAnalysis.py`. [Here](https://docs.docker.com/engine/install/) the instructions. It is available for Windows, Mac and Linux. A Desktop version is available [here](https://docs.docker.com/get-docker/).

### Execution
Once all the previous points are done, from the main folder you can execute in the terminal:

**Only for the first time, Docker image generation**
```
cd <repo_path>/python_scripts/dependencies/GMAT_Application && docker build -t gmat:gui .
```

:exclamation: note that the `.` after `gmat:gui` is mandatory, or the Docker image won't be generated. If you did everything correctly, you should have a `gmat:gui` image waiting to be run.

**After Docker image generation**
Make sure to replace the paths indicated in point :one:. After that you can run, from the current folder:

```
cd python_scripts   # move to python_scripts directory
                    # you should be in <repo_path/python_scripts>

python --version    # check your python version
                    # Python >= 3.10 is needed

conda create --name gaphor python=3.10.12
conda activate gaphor

conda install -c anaconda pyyaml 
conda install pip
pip install graphviz

# run the mission analysis tool with heo_model example
# make sure that the runGaphorParser and runGmat variables in the generalMissionAnalysis.py are set to True

python3 generalMissionAnalysis.py

# parser outputs will be generated in the gaphor_parser_out/ directory
# data and power budgets will be generated in the Output_Lite directory
``` 

:exclamation: If everything runs correctly, you should have in `Output_Lite/Figures` a `BatteryRemainingPower` and a `DataBudget` files,  and in `gaphor_parser_out/` the above mentioned parser outputs.
